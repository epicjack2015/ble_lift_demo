#include <string.h>
#include <jni.h>
#include <android/log.h>
#include<stdio.h>
#include<stdlib.h>
#include "timetec_crypto.h"

JNIEXPORT void JNICALL
Java_com_example_hanif_liftsensor_DeviceControlActivity_initialize( JNIEnv* env, jobject thiz) {


    vCRYPTO_INI_Initialization();
    //vCRYPTO_CTL_MainHandler();
    //vCRYPTO_ACT_Decryption(&data1, &data2);
}

JNIEXPORT void JNICALL
Java_com_example_hanif_liftsensor_DeviceControlActivity_generateSeed( JNIEnv* env, jobject thiz ,
                                                                      jlong cipher_key1,jlong cipher_key2,jlong cipher_key3,jlong cipher_key4,jlong cipher_key5, jlong cipher_key6) {

    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS1", "%i", cipher_key1);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS2", "%i", cipher_key2);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS3", "%i", cipher_key3);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS4", "%i", cipher_key4);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS5", "%i", cipher_key5);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS6", "%i", cipher_key6);

    unsigned char dec_data[6];

    dec_data[0] = cipher_key1;
    dec_data[1] = cipher_key2;
    dec_data[2] = cipher_key3;
    dec_data[3] = cipher_key4;
    dec_data[4] = cipher_key5;
    dec_data[5] = cipher_key6;

    vCRYPTO_ACT_GenerateSeed(&dec_data);
}

JNIEXPORT jstring JNICALL
Java_com_example_hanif_liftsensor_DeviceControlActivity_encryptData( JNIEnv* env, jobject thiz , jlong data1 , jlong data2) {

    char buf[64];
    char buf1[64];
    char buf3[64];
    jstring retval;

    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS_EN", "%i", data1);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS2_EN", "%i", data2);

    vCRYPTO_ACT_Encryption((uint32_t *) data1, (uint32_t *) data2);

    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS_RESULT29", "%i", unCRYPTO_DAT_TmpV0_Buffer.un32); // need to convert back
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS_RESULT92", "%i", unCRYPTO_DAT_TmpV1_Buffer.un32); // need to convert back


    sprintf(buf, "%d", unCRYPTO_DAT_TmpV0_Buffer.un32);
    sprintf(buf1, "%d", unCRYPTO_DAT_TmpV1_Buffer.un32);


    char s3[100]= "";
    char comma[100] = ",";
    strcat(s3,buf);
    strcat(s3,comma);
    strcat(s3,buf1);
    //(*env)->ReleaseStringUTFChars(env,temp1,temp2);
    //temp3 = (*env)->NewStringUTF(env, s3);

    sprintf(buf3, "%s", s3);
    retval = (*env)->NewStringUTF(env,buf3);
    //

    //retval = (*env)->NewStringUTF(env, concatenated);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS_RESULT91", "%p", retval);

    //(*env)->NewStringUTF(env, buf);


    return retval;
    //return string here//
}

JNIEXPORT void JNICALL
Java_com_example_hanif_liftsensor_DeviceControlActivity_decryptData( JNIEnv* env, jobject thiz , jlong data1 , jlong data2) {

    //vCRYPTO_ACT_Encryption((uint32_t *) &data1, (uint32_t *) &data2);

    vCRYPTO_ACT_Decryption((uint32_t *) data1, (uint32_t *) data2);
}

