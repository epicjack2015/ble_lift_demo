/*******************************************************************
#ifdef DOC
File Name	: timetec_crypto.h
Function 	: TimeTec Cryptographic Function
Remark		: none
Date			: 2017/12/30
Copyright	: Henry Cheong
Creator		: Henry Cheong
#endif
********************************************************************/
/***************************************
		Include File
***************************************/
#include <string.h>
#include <jni.h>
#include "timetec_crypto.h"
#include <android/log.h>


/***************************************
		Macro Definition
***************************************/
#define bool int
#define true 1
#define false 0

/***************************************
		Variable Declaration
***************************************/
uint8_t ucCRYPTO_DAT_FixSeed[SEED_LENGTH];

NIBBLE_32b_t unCRYPTO_DAT_TmpV0_Buffer;
NIBBLE_32b_t unCRYPTO_DAT_TmpV1_Buffer;

/***************************************
		Function Prototype Declaration
***************************************/
void vCRYPTO_INI_Initialization(void);

void vCRYPTO_CTL_MainHandler(void);

void vCRYPTO_ACT_Encryption(uint32_t *dat0, uint32_t *dat1);
void vCRYPTO_ACT_Decryption(uint32_t *dat0, uint32_t *dat1);
void vCRYPTO_ACT_GenerateSeed(uint8_t* cipher_key);

void qn902x_tt_encipher(uint8_t bcnt, uint32_t *v0, uint32_t *v1, uint8_t *seed64);
void qn902x_tt_decipher(uint8_t bcnt, uint32_t *v0, uint32_t *v1, uint8_t *seed64);


/****************************************************************************
#ifdef DOC
DESCRIPTION	: Initialize Cryptographic Handler
INPUT				: void
OUTPUT			: void
RETURN			: void
DATE				: 2017/12/30
#endif
 *****************************************************************************/
void vCRYPTO_INI_Initialization(void)
{
    memset(&ucCRYPTO_DAT_FixSeed, 0x00, SEED_LENGTH);

    unCRYPTO_DAT_TmpV0_Buffer.un32 = 0;
    unCRYPTO_DAT_TmpV1_Buffer.un32 = 0;
}

/****************************************************************************
#ifdef DOC
DESCRIPTION	: Cryptographic Main Handler
INPUT				: void
OUTPUT			: void
RETURN			: void
DATE				: 2017/12/30
#endif
 *****************************************************************************/
void vCRYPTO_CTL_MainHandler(void)
{
    bool test;

    test = true;

    if(test == true){
        test = false;

        ucCRYPTO_DAT_FixSeed[0] = 0x08;
        ucCRYPTO_DAT_FixSeed[1] = 0x7C;
        ucCRYPTO_DAT_FixSeed[2] = 0xBE;
        ucCRYPTO_DAT_FixSeed[3] = 0xB2;
        ucCRYPTO_DAT_FixSeed[4] = 0x23;
        ucCRYPTO_DAT_FixSeed[5] = 0x1A;

//		unCRYPTO_DAT_TmpV0_Buffer.nb8.bo_lo = 0x11;
//		unCRYPTO_DAT_TmpV0_Buffer.nb8.bo_hi = 0x22;
//		unCRYPTO_DAT_TmpV0_Buffer.nb8.up_lo = 0x33;
//		unCRYPTO_DAT_TmpV0_Buffer.nb8.up_hi = 0x44;
//		unCRYPTO_DAT_TmpV1_Buffer.nb8.bo_lo = 0xaa;
//		unCRYPTO_DAT_TmpV1_Buffer.nb8.bo_hi = 0xbb;
//		unCRYPTO_DAT_TmpV1_Buffer.nb8.up_lo = 0xcc;
//		unCRYPTO_DAT_TmpV1_Buffer.nb8.up_hi = 0xdd;
        unCRYPTO_DAT_TmpV0_Buffer.nb8.up_hi = 0x14;
        unCRYPTO_DAT_TmpV0_Buffer.nb8.up_lo = 0x08;
        unCRYPTO_DAT_TmpV0_Buffer.nb8.bo_lo = 0xBE;
        unCRYPTO_DAT_TmpV1_Buffer.nb8.up_hi = 0xB2;
        unCRYPTO_DAT_TmpV1_Buffer.nb8.up_lo = 0x23;
        unCRYPTO_DAT_TmpV1_Buffer.nb8.bo_hi = 0x1A;
        unCRYPTO_DAT_TmpV1_Buffer.nb8.bo_lo = 0xC1;

        vCRYPTO_ACT_Encryption(&unCRYPTO_DAT_TmpV0_Buffer.un32, &unCRYPTO_DAT_TmpV1_Buffer.un32);

        vCRYPTO_ACT_Decryption(&unCRYPTO_DAT_TmpV0_Buffer.un32, &unCRYPTO_DAT_TmpV1_Buffer.un32);
    }
}

/****************************************************************************
#ifdef DOC
DESCRIPTION	: Cryptographic Handler - Encryption
INPUT				: *dat0, *dat1
OUTPUT			: *dat0, *dat1
RETURN			: void
DATE				: 2017/12/30
#endif
 *****************************************************************************/
void vCRYPTO_ACT_Encryption(uint32_t *dat0, uint32_t *dat1)
{
    uint8_t round;

    for(round=0;round<num_rounds;round++)
    {
        // qn902x_tt_encipher(round, &unCRYPTO_DAT_TmpV0_Buffer.un32, &unCRYPTO_DAT_TmpV1_Buffer.un32, ucCRYPTO_DAT_FixSeed);	//encryption
        qn902x_tt_encipher(round, (uint32_t *) &dat0, (uint32_t *) &dat1, ucCRYPTO_DAT_FixSeed); //encryption
    }

    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS121", "%s", (char *) &dat0);
    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS122", "%s", (char *) &dat1);

    unCRYPTO_DAT_TmpV0_Buffer.un32 = dat0;
    unCRYPTO_DAT_TmpV1_Buffer.un32 = dat1;

}

/****************************************************************************
#ifdef DOC
DESCRIPTION	: Cryptographic Handler - Decryption
INPUT				: *dat0, *dat1
OUTPUT			: *dat0, *dat1
RETURN			: void
DATE				: 2017/12/30
#endif
 *****************************************************************************/
void vCRYPTO_ACT_Decryption(uint32_t *dat0, uint32_t *dat1)
{
    uint8_t round;

    for(round=num_rounds;round>0;round--)
    {
        //qn902x_tt_decipher(round, &unCRYPTO_DAT_TmpV0_Buffer.un32, &unCRYPTO_DAT_TmpV1_Buffer.un32, ucCRYPTO_DAT_FixSeed);	//decryption
        qn902x_tt_decipher(round, (uint32_t *) &dat0, (uint32_t *) &dat1, ucCRYPTO_DAT_FixSeed);	//decryption
        __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS123", "%s", (char *) &dat0);
        __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS124", "%s", (char *) &dat1);
    }
}

/****************************************************************************
#ifdef DOC
DESCRIPTION	: Cryptographic Handler - Generate seed from key
INPUT				: key
OUTPUT			: void
RETURN			: void
DATE				: 2017/12/30
#endif
 *****************************************************************************/

void vCRYPTO_ACT_GenerateSeed(uint8_t* cipher_key)
{
    memcpy(ucCRYPTO_DAT_FixSeed, cipher_key, SEED_LENGTH); //Use key as decryption seed

//    ucCRYPTO_DAT_FixSeed[0] = 0x40;
//    ucCRYPTO_DAT_FixSeed[1] = 0x4E;
//    ucCRYPTO_DAT_FixSeed[2] = 0x36;
//    ucCRYPTO_DAT_FixSeed[3] = 0x85;
//    ucCRYPTO_DAT_FixSeed[4] = 0x32;
//    ucCRYPTO_DAT_FixSeed[5] = 0xAD;

    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS", "%s", ucCRYPTO_DAT_FixSeed);
//    __android_log_print(ANDROID_LOG_DEBUG, "TRACKERS5", "%s", cipher_key);

}

/****************************************************************************
#ifdef DOC
DESCRIPTION	: Cryptographic Handler - Enryption
INPUT				: bcnt = encryption loop counter
							*v0(low16bits), *v1(high16bits)
							*seed64 = array 4 of 16bit buffer, 64 bits SEED
OUTPUT			: *v0, *v1
RETURN			: void
DATE				: 2017/12/30
#endif
 *****************************************************************************/
void qn902x_tt_encipher(uint8_t bcnt, uint32_t *v0, uint32_t *v1, uint8_t *seed64)
{
    uint16_t sum ;
    sum = 0 ;

    *v0 += (((*v1 << shiftLeft) ^ (*v1 >> shiftRight)) + *v1) ^ (sum + *(seed64+(sum)));
    sum += delta16;
    *v1 += (((*v0 << shiftLeft) ^ (*v0 >> shiftRight)) + *v0) ^ (sum + *(seed64+((sum>>shiftSum) & 0x3)));
}

/****************************************************************************
#ifdef DOC
DESCRIPTION	: Cryptographic Handler - Decryption
INPUT				: bcnt = decryption loop counter
							*v0(low16bits), *v1(high16bits)
							*seed64 = array 4 of 16bit buffer, 64 bits SEED
OUTPUT			: *v0, *v1
RETURN			: void
DATE				: 2017/12/30
#endif
 *****************************************************************************/
void qn902x_tt_decipher(uint8_t bcnt, uint32_t *v0, uint32_t *v1, uint8_t *seed64)
{
    uint16_t sum ;
    sum = delta16;

    *v1 -= (((*v0 << shiftLeft) ^ (*v0 >> shiftRight)) + *v0) ^ (sum + *(seed64+((sum>>shiftSum) & 0x3)));
    sum -= delta16;
    *v0 -= (((*v1 << shiftLeft) ^ (*v1 >> shiftRight)) + *v1) ^ (sum + *(seed64+(sum & 0x3)));
}

/* --------------------------------- End Of File ------------------------------ */
