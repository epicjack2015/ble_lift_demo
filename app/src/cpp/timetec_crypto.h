/*******************************************************************
#ifdef DOC
File Name	: timetec_crypto.h
Function 	: TimeTec Cryptographic Function Header File
Remark		: none
Date			: 2017/12/30
Copyright	: Henry Cheong
Creator		: Henry Cheong
#endif
********************************************************************/

#ifndef __TIMETEC_CRYPTO_H
#define __TIMETEC_CRYPTO_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/***************************************
		Macro Definition
***************************************/
#define 	num_rounds     	(5)          			/* encryption/decryption round */
#define 	shiftLeft     	(3)           		/* encryption/decryption shift left counter */
#define 	shiftRight    	(4)           		/* encryption/decryption shift right counter */
#define 	shiftSum      	(7)           		/* encryption/decryption shift sum counter */
#define 	delta16       	(0x79B9)     			/* encryption/decryption magic number */

#define 	SEED_LENGTH			6

typedef union NIBBLE_32b {
    struct NB32 {
        uint8_t	bo_lo;		//LSB
        uint8_t	bo_hi;
        uint8_t	up_lo;
        uint8_t	up_hi;		//MSB
    }nb8;
    uint32_t un32;
}NIBBLE_32b_t;

/***************************************
		Variable Declaration
***************************************/
extern uint8_t ucCRYPTO_DAT_FixSeed[SEED_LENGTH];

extern NIBBLE_32b_t unCRYPTO_DAT_TmpV0_Buffer;
extern NIBBLE_32b_t unCRYPTO_DAT_TmpV1_Buffer;

/***************************************
		Function Prototype Declaration
***************************************/
extern void vCRYPTO_INI_Initialization(void);
extern void vCRYPTO_CTL_MainHandler(void);

extern void vCRYPTO_ACT_Encryption(uint32_t *dat0, uint32_t *dat1);
extern void vCRYPTO_ACT_Decryption(uint32_t *dat0, uint32_t *dat1);
extern void vCRYPTO_ACT_GenerateSeed(uint8_t* cipher_key);

#ifdef __cplusplus
}
#endif

#endif	//__TIMETEC_CRYPTO_H
