package com.example.hanif.liftsensor;

/**
 * Created by HANIF on 8/10/2017.
 */
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hanif.liftsensor.DeviceControlActivity;
import com.example.hanif.liftsensor.R;

import java.util.ArrayList;
import java.util.List;

public class DeviceScanActivity extends ListActivity {

    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private BluetoothGatt mGatt;
    private boolean mScanning;
    private boolean mBluetoothStatus = false;
    private List<Integer> mRSSIs;
    private List<ScanResult> mScanResults;
    private List<byte[]> mScanResultsDep;
    private Handler mHandler;
    private ScanCallback mScanCallback;
    private BluetoothAdapter.LeScanCallback mLeScanCallback;
    private final String TAG = "DeviceScanActivity";

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_ENABLE_LOCATION = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                    case BluetoothAdapter.STATE_OFF:
                        //case BluetoothAdapter.STATE_TURNING_OFF:
                        mBluetoothStatus = true;
                        new AlertDialog.Builder(context)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.error_bluetooth_off)
                                .setIcon(R.mipmap.ic_launcher)
                                .setCancelable(false)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(DeviceScanActivity.this, R.string.error_bluetooth_off, Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                })
                                .show();
                        //finish();
                        break;

                    case BluetoothAdapter.STATE_ON:
                        //case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getActionBar().setTitle(R.string.app_name);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }

        mHandler = new Handler();


        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Log.i("Permission", "Granted");

                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        //startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_ENABLE_LOCATION);
                    }
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                setListAdapter(mLeDeviceListAdapter);
                mRSSIs.clear();
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        mRSSIs = new ArrayList<Integer>();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            mScanResultsDep = new ArrayList<byte[]>();
        else
            mScanResults = new ArrayList<ScanResult>();

        setListAdapter(mLeDeviceListAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, R.string.error_bluetooth_off, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        else {

            // Initializes list view adapter.
            mLeDeviceListAdapter = new LeDeviceListAdapter();
            mRSSIs = new ArrayList<Integer>();
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                mScanResultsDep = new ArrayList<byte[]>();
            else
                mScanResults = new ArrayList<ScanResult>();

            setListAdapter(mLeDeviceListAdapter);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder() //
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) //
                        .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES) //
                        .build();

                ParcelUuid puuid = ParcelUuid.fromString("f05abac0-3936-11e5-87a6-0002a5d5c51b");

                filters = new ArrayList<ScanFilter>();

                //filter for BLE lift
                ScanFilter filter = new ScanFilter.Builder()
                        //.setDeviceName(null)
                        //.setManufacturerData((int) 0xcd, new byte[]{ (byte) 0xcd, (byte) 0x00, (byte) 0xfe, (byte) 0x14, (byte) 0xad, (byte) 0x11, (byte) 0xcf, (byte) 0x40, (byte) 0x06, (byte) 0x3f, (byte) 0x11, (byte) 0xe5, (byte) 0xbe, (byte) 0x3e, (byte) 0x00, (byte) 0x02, (byte) 0xa5, (byte) 0xd5, (byte) 0xc5, (byte) 0x1b })
                        .setManufacturerData((int) 0xcd, new byte[]{-2, 20, -83, 17, -49, 64, 6, 63, 17, -27, -66, 62, 0, 2, -91, -43, -59})    //, 27})
                        //.setServiceUuid(ParcelUuid.fromString("f05abac0-3936-11e5-87a6-0002a5d5c51b")) //"f05abac1-3936-11e5-87a6-0002a5d5c51b")) //("1bc5d5a5-0200-a687-e511-3639c0ba5af0")) //

                        .build();

                filters.add(filter);
                Log.d(TAG, "onAdd" + filters);

                //filter for BLE card
                filter = new ScanFilter.Builder()
                        .setDeviceName("Quintic BLE")
                        .build();

                filters.add(filter);

            }

            scanLeDevice(true);



        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onStart() {
        super.onStart();

        setLECallbacks();

        registerReceiver(bluetoothReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled() && !mBluetoothStatus) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder() //
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) //
                        .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES) //
                        .build();

                ParcelUuid puuid = ParcelUuid.fromString("f05abac0-3936-11e5-87a6-0002a5d5c51b");

                filters = new ArrayList<ScanFilter>();

                //filter for BLE lift
                ScanFilter filter = new ScanFilter.Builder()
                        //.setDeviceName(null)
                        //.setManufacturerData((int) 0xcd, new byte[]{ (byte) 0xcd, (byte) 0x00, (byte) 0xfe, (byte) 0x14, (byte) 0xad, (byte) 0x11, (byte) 0xcf, (byte) 0x40, (byte) 0x06, (byte) 0x3f, (byte) 0x11, (byte) 0xe5, (byte) 0xbe, (byte) 0x3e, (byte) 0x00, (byte) 0x02, (byte) 0xa5, (byte) 0xd5, (byte) 0xc5, (byte) 0x1b })
                        .setManufacturerData((int) 0xcd, new byte[]{-2, 20, -83, 17, -49, 64, 6, 63, 17, -27, -66, 62, 0, 2, -91, -43, -59})    //, 27})
                        //.setServiceUuid(ParcelUuid.fromString("f05abac0-3936-11e5-87a6-0002a5d5c51b")) //"f05abac1-3936-11e5-87a6-0002a5d5c51b")) //("1bc5d5a5-0200-a687-e511-3639c0ba5af0")) //

                        .build();

                filters.add(filter);
                Log.d(TAG, "onAdd" + filters);

                //filter for BLE card
                filter = new ScanFilter.Builder()
                        .setDeviceName("Quintic BLE")
                        .build();

                filters.add(filter);

                /*//filter for BLE card 1
                filter = new ScanFilter.Builder()
                        .setDeviceAddress("08:7C:BE:B2:23:1C")
                        .build();

                filters.add(filter);

                //filter for BLE card 2
                filter = new ScanFilter.Builder()
                        .setDeviceAddress("08:7C:BE:B2:B1:71")
                        .build();

                filters.add(filter);

                //filter for BLE card 3
                filter = new ScanFilter.Builder()
                        .setDeviceAddress("08:7C:BE:B2:B1:6F")
                        .build();

                filters.add(filter);*/

            }

            scanLeDevice(true);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();

        scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBluetoothAdapter.isEnabled()) {

            //resetLECallbacks();
            scanLeDevice(false);
            mLeDeviceListAdapter.clear();
            mRSSIs.clear();
            unregisterReceiver(bluetoothReceiver);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        //position--;
        final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
        if (device == null) return;
        final Intent intent = new Intent(this, DeviceControlActivity.class);
        mLeDeviceListAdapter.getItem(position);
        intent.putExtra("DEVICE_REAL_ADDRESS",device.getAddress());
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(DeviceControlActivity.EXTRAS_SCAN_RESULT, mScanResultsDep.get(position));
            //Log.d(TAG, mScanResults.get(position).toString());
        }
        else{
            intent.putExtra(DeviceControlActivity.EXTRAS_SCAN_RESULT, mScanResults.get(position));
            Log.d(TAG, String.valueOf(mScanResults.get(position)));
        }
        if (mScanning) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                //handle null pinter exception for API >= 21
                if(mBLEScanner != null)
                    mBLEScanner.stopScan(mScanCallback);
            }
            mScanning = false;
        }
        startActivity(intent);
        finish();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mLeDeviceListAdapter.isEmpty() && mScanning) {
                        Toast.makeText(DeviceScanActivity.this, R.string.ble_scan_empty, Toast.LENGTH_SHORT).show();
                    }
                    mScanning = false;
                    if (mBluetoothAdapter.isEnabled()) {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                            mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        } else {
                            //handle null pinter exception for API >= 21
                            if(mBLEScanner != null)
                                mBLEScanner.stopScan(mScanCallback);
                        }
                        invalidateOptionsMenu();
                    }
                }
            }, SCAN_PERIOD);

            mScanning = true;

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                //handle null pinter exception for API >= 21
                if(mBLEScanner != null)
                    mBLEScanner.startScan(filters, settings, mScanCallback);
            }
        } else {
            mScanning = false;
            if (mBluetoothAdapter.isEnabled()) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                } else {
                    //handle null pinter exception for API >= 21
                    if(mBLEScanner != null)
                        mBLEScanner.stopScan(mScanCallback);
                }
            }
        }
        invalidateOptionsMenu();
    }


    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator =  DeviceScanActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device, int rssi, ScanResult scanResult) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
                mRSSIs.add(rssi);
                mScanResults.add(scanResult);
            } else {
                mRSSIs.remove(mLeDevices.indexOf(device));
                mRSSIs.add(mLeDevices.indexOf(device), rssi);
                mScanResults.remove(mLeDevices.indexOf(device));
                mScanResults.add(mLeDevices.indexOf(device), scanResult);
            }
        }

        public void addDeviceDep(BluetoothDevice device, int rssi, byte[] scanResult) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
                mRSSIs.add(rssi);
                mScanResultsDep.add(scanResult);
            } else {
                mRSSIs.remove(mLeDevices.indexOf(device));
                mRSSIs.add(mLeDevices.indexOf(device), rssi);
                mScanResultsDep.remove(mLeDevices.indexOf(device));
                mScanResultsDep.add(mLeDevices.indexOf(device), scanResult);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.activity_device_scan, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.deviceRSSI = (TextView) view.findViewById(R.id.device_rssi);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceAddress.setText(device.getAddress());
            viewHolder.deviceRSSI.setText("RSSI: " + mRSSIs.get(i) + "dB");
//            viewHolder.deviceRSSI.setText("RSSI: " + mRSSIs.get(i) + "dB");
            //ImageView iV = (ImageView) view.findViewById(R.id.image_rssi);
            //iV.setImageResource(R.drawable.rssi_strength100);

            /*int rssi = mRSSIs.get(i);
            if (rssi < -27 && rssi > -110) {
                if (rssi <= -27 && rssi > -60)
                    ((ImageView) view.findViewById(R.id.image_rssi)).setImageResource(R.drawable.rssi_strength100);

                if (rssi <= -60 && rssi > -70)
                    ((ImageView) view.findViewById(R.id.image_rssi)).setImageResource(R.drawable.rssi_strength75);

                if (rssi <= -70 && rssi > -80)
                    ((ImageView) view.findViewById(R.id.image_rssi)).setImageResource(R.drawable.rssi_strength50);

                if (rssi <= -80 && rssi > -90)
                    ((ImageView) view.findViewById(R.id.image_rssi)).setImageResource(R.drawable.rssi_strength25);

                if (rssi <= -90 && rssi > -110)
                    ((ImageView) view.findViewById(R.id.image_rssi)).setImageResource(R.drawable.rssi_strength0);
            } else
                ((ImageView) view.findViewById(R.id.image_rssi)).setImageResource(R.drawable.rssi_strengthnil);*/

            return view;
        }
    }


    private void setLECallbacks()
    {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            // Device scan callback.
            mLeScanCallback =
                    new BluetoothAdapter.LeScanCallback() {

                        @Override
                        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                            Log.i("Result + RSSI", scanRecord.toString() + rssi);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mLeDeviceListAdapter.addDeviceDep(device, rssi, scanRecord);
                                    mLeDeviceListAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    };

        } else {
            mScanCallback = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, final ScanResult result) {
                    Log.i("Result + RSSI", result.toString() + result.getRssi());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            mLeDeviceListAdapter.addDevice(result.getDevice(), result.getRssi(), result);
                            mLeDeviceListAdapter.notifyDataSetChanged();
                        }
                    });

                    //super.onScanResult(callbackType, result);
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    //for (ScanResult sr : results) {
                    //    Log.i("Scan Result", sr.toString());
                    //}
                    //super.onBatchScanResults(results);
                }

                @Override
                public void onScanFailed(int errorCode) {
                    //Log.i("Scan Failed", "Code:" + errorCode);
                    //super.onScanFailed(errorCode);
                }
            };
        }
    }

    void resetLECallbacks() {
        mLeScanCallback = null;
    }

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        TextView deviceRSSI;
    }
}
