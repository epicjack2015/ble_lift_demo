package com.example.hanif.liftsensor;

/**
 * Created by jack on 24-Aug-15.
 */

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;


//import org.apache.commons.lang.StringEscapeUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static android.text.format.DateUtils.DAY_IN_MILLIS;
import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static android.text.format.DateUtils.WEEK_IN_MILLIS;

public final class CommonUtilities {
    // debug will change webservice and db access to test
    public static final boolean TEST = false;
    public static final boolean DEBUG = true;
    public static final boolean DISABLEPING = true; // set to true if dont want to ping
    public static final boolean EMULATOR = false;
    public static boolean DISPLAYONCEGREET = false;
    public static int notificationCount = 0;
    public static String API_KEY = "AIzaSyDOR9L77ftSgAu2mHtReFNr5cFg7hkIZHI";
    public static String SHORTEN_URL_API = "https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY;
    public static final String APPLOCK_PACKAGE_NAME = "com.epicamera.vms.app_lock";

    public static String HOST = "https://test.i-neighbour.com";
    public static String WEBSITE = "https://test.i-neighbour.com";//for the user visible link
    public static String IADS = "https://test.iadhub.com";
    public static String IMERCHANT = "https://merchanttest.i-neighbour.com";

    /*public static String HOST = "https://app.i-neighbour.com";
    public static String WEBSITE = "https://www.i-neighbour.com";//for the user visible link
    public static String IADS = "https://www.iadhub.com";
    public static String IMERCHANT = "https://merchant.i-neighbour.com";*/


    public static boolean cameraAccessGranted = false;
    public static boolean storageAccessGranted = false;
    public static boolean fineLocationAccessGranted = false;
    public static boolean phoneAccessGranted = false;
    public static boolean forceCloseActivity = false;
    public static boolean contactsAccessGranted = false;
    public static boolean smsAccessGranted = false;

    public static boolean isAdvertised = false, callAdvertisement = true;

    // Sharedpref file name
    public static final String PREF_NAME = "iNeighbourAppPref";

    // give your server registration url here
    public static final String SERVER_URL = "http://i-neighbour.com/gcm_server_php2/register.php";

    // Google project id
    //public static final String SENDER_ID = "405713333530";
    public static final String SENDER_ID = "979193133928";

//    public static List<NavDrawerItemNew> listDrawerParent = new ArrayList<>();
//    public static HashMap<String, List<NavDrawerItemChildNew>> listDrawerChild = new HashMap<>();
//    public static CustomExpandAdapter customDrawerAdapter;
//
//    public static List<NavDrawerItemNew> listDrawerParent2 = new ArrayList<>();
//    public static HashMap<String, List<NavDrawerItemNew>> listDrawerChild2 = new HashMap<>();
//    public static CustomMoreMenuAdapter customMoreMenuAdapter;


    public static boolean boomGateNearbyIsRunning = false;

    // stop handler from
    public static Boolean stoprunning = false;

    public static String sDeviceType, sDeviceModel, sChannel, sEnable, sDeviceId;
    /**
     * Tag used on log messages.
     */
    public static final String TAG = "iNeighbour GCM";

    public static final String DISPLAY_MESSAGE_ACTION =
            "com.epicamera.vms.i_neighbour.DISPLAY_MESSAGE";

    public static final String EXTRA_MESSAGE = "message";

    private static ProgressDialog progressDialog;

    private static CountDownTimer timer;
    public static CountDownTimer advTimer, advDataTimer;
    public static ArrayList<HashMap<String, String>> allowAdvertiseList = new ArrayList<>();
    public static boolean isTimerRunning = false;

    // to prevent any double clicked
    public static boolean flagEnable = true;

    /**
     * Shows the progress message.
     */
    public static void showProgress(Context context, String sMessage) {
        if (progressDialog == null || progressDialog.getContext() != context) {
            progressDialog = new ProgressDialog(context);
        }
//        progressDialog.setMessage(sMessage); //commented 7/8/17
        progressDialog.setCancelable(false);
//            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });

        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        progressDialog.show();
        //progressDialog.setContentView(R.layout.loading_indicator_view);
    }

    public static void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static void showAlertDialog(final Activity activity, String message, final boolean finish) {
//        AlertDialog alertDialog;
//        alertDialog = new AlertDialog.Builder(activity).create();
//        alertDialog.setMessage(message);
//        alertDialog.setCancelable(false);
//
//        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(R.string.txt_action_ok),
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        if (finish)
//                            activity.finish();
//                    }
//                });
//        alertDialog.show();

        // get prompts.xml view
        final android.app.AlertDialog ShowAlertDialog;
        LayoutInflater li = LayoutInflater.from(activity);
        //View promptsView = li.inflate(R.layout.dialog_common, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);


        alertDialogBuilder.setCancelable(false);



        //rounded corner dialog for LOLLIPOP and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            llRoot.setBackgroundResource(R.drawable.common_dialog);
//            btnOk.setBackgroundResource(R.drawable.common_dialog_button);
        }

        try {
            //imgPhoto.setImageResource(R.drawable.icn_dashboard_ineighbour);

            // txtMsg.setText(message);
        } catch (Exception e) {
            Log.e("CommonUtilities", "" + e.getMessage());
        }

        // create alert dialog
        ShowAlertDialog = alertDialogBuilder.create();
        ShowAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // show it
        ShowAlertDialog.show();
    }


    public static void toggleRotation(Activity activity, Boolean rotate) {
        // disable/enable orientation of screen
        if (rotate) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        } else {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }
    }


    //for MD5
    public static String MD5(String md5) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            //below 'for' is equals to for (int i = 0; i < array.length; ++i) {
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            Log.d(TAG, "MD5 Error:" + e);
        }
        return null;
    }

    // for email
    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static String file_size(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.2f %sB", bytes / Math.pow(unit, exp), pre);
    }


    public static boolean isEmulator() {
        return (Build.FINGERPRINT.contains("generic") || Build.BRAND.equalsIgnoreCase("generic"))
                || Build.MANUFACTURER.contains("Genymotion");
    }


    public static boolean pingHost() {
        boolean reachable = false;
        if (isEmulator()) {
            reachable = true;
        } else if (DISABLEPING) {
            reachable = true;
        } else {
            String host;
            host = ((CommonUtilities.HOST).replace("https://", "")).replace("http://", "").replace("www.", "").replace("test.", "");

            String cmd = "ping -c 1 " + host;
            try {
                Process p1 = java.lang.Runtime.getRuntime().exec(cmd);
                int returnVal = p1.waitFor();
                reachable = (returnVal == 0); // success == 0, otherwise == 2
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return reachable;
    }

    public static boolean isConnectionAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()
                    && netInfo.isConnectedOrConnecting()
                    && netInfo.isAvailable()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isInternetAvailable(Context context) {
        boolean reachable = false;

        String host = "google.com";

        String cmd = "ping -c 1 " + host;
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec(cmd);
            int returnVal = p1.waitFor();
            reachable = (returnVal == 0); // success == 0, otherwise == 2
        } catch (Exception e) {
            e.printStackTrace();
        }

        return reachable;
    }

    public static void checkConnectionStatus(Activity activity) {
        String message;
        if (CommonUtilities.isConnectionAvailable(activity)) {
            if (CommonUtilities.pingHost()) {
                message = "Connected";
            } else {
                //message = activity.getString(R.string.lost_connection_to_host);
            }
        } else {
           // message = activity.getString(R.string.no_internet_connection);
        }

        //if (DEBUG) CommonUtilities.showAlertDialog(activity, message, false);

        /*
        if(!message.equals("Connected")){
            Intent intent = new Intent(activity, NoConnectionActivity.class);
            intent.putExtra("className", activity.getClass().getName());
            intent.putExtra("message",message);
            activity.startActivity(intent);
            activity.finish();
        }
        */
    }

    public static String randomString(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


    //Set Image rotation same as stored image in server
    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {

            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }


    // pad with leading zero value
    public static String pad_with_zeroes(String item, int length) {

        String my_string = "" + item;
        while (my_string.length() < length) {
            my_string = '0' + my_string;
        }

        return my_string;

    }

    //Make Qr Code with company logo in the center
    public static Bitmap makeQrcodeCenterLogo(Bitmap bmp, Bitmap overlay) {

        Bitmap bmOverlay = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp, new Matrix(), null);
        // canvas.drawBitmap(bmp2, new Matrix(), null);
        //Make the logo in center
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        float centerX = (width - overlay.getWidth()) * 0.5f;
        float centerY = (height - overlay.getHeight()) * 0.5f;

        canvas.drawBitmap(overlay, centerX, centerY, null);
        return bmOverlay;
    }


    public static String convertHexToString(String hex) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }

        return sb.toString();
    }

    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        if (hexString.length() % 2 != 0) {
            hexString = hexString.substring(0, hexString.length() - 1)
                    + "0"
                    + hexString.substring(hexString.length() - 1,
                    hexString.length());
        }

        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }


    public static String byte2HexStr(byte[] b, int len) {
        String stmp = "";
        StringBuilder sb = new StringBuilder("");
        for (int n = 0; n < len; n++) {
            stmp = Integer.toHexString(b[n] & 0xFF);
            sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
            //sb.append(" ");
        }
        return sb.toString().toUpperCase().trim();
    }


    /* Store QRCode */
    public static void storeCQImage(Bitmap image) {
        File pictureFile = null;
        try {
            pictureFile = getOutputMediaFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
            Log.e(TAG, "ERROR :" + ex.getMessage());
        }
        if (pictureFile == null) {
            Log.d(TAG, "Error creating media file, check storage permissions: ");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }//END storeCQImage

    /**
     * Create a File for saving an image or video
     */
    public static File getOutputMediaFile() throws IOException {

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());

        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory()
                + "/i-Neighbour";

        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");

        return image;
    }//END getOutputMediaFile
     /* End Store QRCode */


    // get the days between dates
    public static int get_count_of_days(String first_date, String second_date) {
        int days = 0;
        // get the number of days
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date date1 = myFormat.parse(first_date);
            Date date2 = myFormat.parse(second_date);
            long diff = date2.getTime() - date1.getTime();
            long numOfDays = diff / (1000 * 60 * 60 * 24);
            int numberStay = (int) numOfDays;
            days = (int) numOfDays;

            //if return negative value between month
            if (numberStay < 0) {
                String month = (String) android.text.format.DateFormat.format("mm", date1);
                String year = (String) android.text.format.DateFormat.format("yyyy", date1);
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, Integer.parseInt(year));
                cal.set(Calendar.MONTH, Integer.parseInt(month) - 1);
                int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                days = daysInMonth + numberStay;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return days;
    }

    // make to portrait for tablet
    public static void toggleRotationAdvance(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float yInches = metrics.heightPixels / metrics.ydpi;
        float xInches = metrics.widthPixels / metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
        if (diagonalInches >= 6.5) {
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            // ((Activity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            toggleRotation((Activity) context, false);
        }
    }


    // Function to change the date/time format
    public static String ChangeTimeFormat(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    // Function to change the date/time format
    public static String ChangeDateFormat(String dates) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dates);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    // Function to change the date/time format (to fix calendar locale bug)
    public static String ChangeDateFormat2(String dates) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.ENGLISH);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dates);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    // Function to change the date & time format
    public static String ChangeDateTimeFormat(String time) {
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd-MMM-yyyy  hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            if (time != "null") {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    // Function to change the date & time format
    public static String ChangeDateWithoutTimeFormat(String time) {
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            if (time != "null") {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String FirstCharUpper(String text) {
        // Change to Lower Case
        try {
            text = text.toLowerCase();
            text = text.substring(0, 1).toUpperCase() + text.substring(1);
        } catch (Exception exp) {
            Log.e("Error", "Error" + exp.getMessage());
        }

        return text;
    }


    public static boolean isLegalPassword(String pass) {
        if (!pass.matches(".*[A-Z].*")) return false;

        if (!pass.matches(".*[a-z].*")) return false;

        if (!pass.matches(".*\\d.*")) return false;

        return true;
    }

    // to encode to urlencode
    public static String urlencode(String original) {
        try {
            //return URLEncoder.encode(original, "utf-8");
            //fixed: to comply with RFC-3986
            return URLEncoder.encode(original, "utf-8").replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
        } catch (UnsupportedEncodingException e) {
            //  Logger.e(e.toString());
        }
        return null;
    }

    /**
     * Sets ListView height dynamically based on the height of the items.
     *
     * @param listView to be resized
     * @return true if the listView is successfully resized, false otherwise
     */
    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    public static String getRelativeDateTimeByMinute(Context context, String strDateTime) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate;
        long time = 0;
        try {
            convertedDate = dateFormat.parse(strDateTime);
            time = convertedDate.getTime();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        CharSequence relativeDateTimeCharSequence = DateUtils.getRelativeDateTimeString(context, time, MINUTE_IN_MILLIS, WEEK_IN_MILLIS, 0);
        String relativeDateTimeString = String.valueOf(relativeDateTimeCharSequence);

        return relativeDateTimeString;
    }

    //GET RELATIVEDATETIME BY DAY IN MILLIS, WEEK IN MILLIS
    public static String getRelativeDateTimeByDay(Context context, String strDateTime) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate = new Date();
        long time = 0;
        try {
            convertedDate = dateFormat.parse(strDateTime);
            time = convertedDate.getTime();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String relativeDateTimeString = "";
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            //TO SHOW DATETIME FORMAT AS Today, 4:30 PM, Today, 4:30 PM, 2 days ago, 4:30 PM
            Calendar calendar = Calendar.getInstance();
            Date phoneDate = calendar.getTime();

            DateFormat phoneYearFormat = new SimpleDateFormat("yyyy");
            int iPhoneYear = Integer.parseInt(phoneYearFormat.format(phoneDate));

            long phoneTimeMillis = phoneDate.getTime();
            long diffMillis = phoneTimeMillis - time;
            long elapsedDays = (TimeUnit.DAYS).convert(diffMillis, TimeUnit.MILLISECONDS);
            String relativeDate = "";
            if (elapsedDays == 0) {
                //relativeDate = context.getResources().getString(R.string.txt_today);
            } else if (elapsedDays == 1) {
              //  relativeDate = context.getResources().getString(R.string.txt_yesterday);
            } else if (elapsedDays >= 2 && elapsedDays <= 7) {
               // relativeDate = context.getResources().getString(R.string.txt_days_ago).replace("number", Long.toString(elapsedDays));
            } else {
                DateFormat newDateFormat = new SimpleDateFormat("yyyy");
                int convertedYear = Integer.parseInt(newDateFormat.format(convertedDate));
                if (iPhoneYear == convertedYear) {
                    newDateFormat = new SimpleDateFormat("MMM dd");
                    relativeDate = newDateFormat.format(convertedDate);
                } else {
                    final String format = Settings.System.getString(context.getContentResolver(), Settings.System.DATE_FORMAT);
                    if (TextUtils.isEmpty(format)) {
                        newDateFormat = android.text.format.DateFormat.getMediumDateFormat(context.getApplicationContext());
                    } else {
                        newDateFormat = new SimpleDateFormat(format);
                    }
                    relativeDate = newDateFormat.format(convertedDate);
                }
            }

            SimpleDateFormat newTimeFormat = new SimpleDateFormat("hh:mm a");
            String relativeTime = newTimeFormat.format(convertedDate);
            relativeDateTimeString = relativeDate + ", " + relativeTime;
        } else {
            CharSequence relativeDateTimeCharSequence = DateUtils.getRelativeDateTimeString(context, time, DAY_IN_MILLIS, WEEK_IN_MILLIS, 0);
            relativeDateTimeString = String.valueOf(relativeDateTimeCharSequence);
            if (relativeDateTimeString.contains("Today")) {
               // relativeDateTimeString = relativeDateTimeString.replace("Today", context.getResources().getString(R.string.txt_today));
            } else if (relativeDateTimeString.contains("Yesterday")) {
                //relativeDateTimeString = relativeDateTimeString.replace("Yesterday", context.getResources().getString(R.string.txt_yesterday));
            } else if (relativeDateTimeString.contains("days ago")) {

                String[] arr1 = relativeDateTimeString.split(",");
                String[] arr2 = arr1[0].split(" ");
                //relativeDateTimeString = relativeDateTimeString.replace(arr1[0], context.getResources().getString(R.string.txt_days_ago));
                relativeDateTimeString = relativeDateTimeString.replace("[number]", arr2[0]);
            }
        }

        return relativeDateTimeString;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void SimultaneousAsyncTask(AsyncTask<Void, Void, Boolean> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }


    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static void showOrHideBottomMenu(final View view) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
//                int heightDiff = view.getRootView().getHeight() - view.getHeight();
//                if (heightDiff > dpToPx(activity, 200)) { // if more than 200 dp, it's probably a keyboard...
//                    Log.d(TAG,"here keyboard");
//                    bottomNavigation.setVisibility(View.GONE);
//                }else{
//                    Log.d(TAG,"here no keyboard");
//                    bottomNavigation.setVisibility(View.VISIBLE);
//                }

                Rect r = new Rect();
                view.getWindowVisibleDisplayFrame(r);
                int screenHeight = view.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d(TAG, "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    //bottomNavigation.setVisibility(View.GONE);
                } else {
                    // keyboard is closed
                    //bottomNavigation.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    // Function to change the date & time format
    public static String ChangeDateTimeFormatWithCapitalAMPM(String time) {
        String inputPattern = "dd MMM yyyy, hh:mm a";
        String outputPattern = "dd MMM yyyy, hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.ENGLISH);

        Date date = null;
        String str = null;

        try {
            if (time != "null") {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String ChangeDateTimeFormatWithCapitalAMPM2(String time) {
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd MMM yyyy, hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            if (time != "null") {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String CustomizeChangeDateTimeFormat(String inputPattern, String outputPattern, String time) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            if (time != "null") {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void ClearCurrentBMIndex(Activity activity, String userType) {
       /* if (userType != null && !userType.isEmpty()) {
            if (userType.equalsIgnoreCase("RESIDENT") || userType.equalsIgnoreCase("RESIDENTOWNER")) {
                ((ResidenceHomeActivityBM) activity).clearCurrentBM();
            } else if (userType.equalsIgnoreCase("STAFF") || userType.equalsIgnoreCase("OWNER")) {
                ((CommitteeHomeActivityBM) activity).clearCurrentBM();
            } else if (userType.equalsIgnoreCase("SECADMIN") || userType.equalsIgnoreCase("SECGUARD") ||
                    userType.equalsIgnoreCase("SECGUARDHS")) {
                ((SecurityHomeActivityBM) activity).clearCurrentBM();
            } else if (userType.equalsIgnoreCase("VISITOR")) {
                ((VisitorMemberHomeActivityBM) activity).clearCurrentBM();
            } else {
                ((IAmVisitorHomeActivityBM) activity).clearCurrentBM();
            }
        } else {
            ((IAmVisitorHomeActivityBM) activity).clearCurrentBM();
        }*/

    }

    //function to set timer for request timeout
    public static void startRequestTimeoutTimer(final AsyncTask task) {
        timer = new CountDownTimer(25000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "ticking " + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                if (task.getStatus() == AsyncTask.Status.PENDING || task.getStatus() == AsyncTask.Status.RUNNING) {
                    task.cancel(true);
                    Log.d(TAG, "Task is cancelled.");
                }
            }
        }.start();
    }

    public static void cancelRequestTimeOutTimer() {
        if (timer != null) {
            Log.d(TAG, "cancel timer");
            timer.cancel();
        }
    }

    public static String encodeInputString(String text) {
       // String escapedString = StringEscapeUtils.escapeJava(text);

        //comment it on 6/3/18 to make sure both ios platform and web platform work well
//        String result;
//        result = escapedString.replace("\\", "\\\\");
//        result = result.replace("'", "\\'");
//
//        result = result.replace("\\\\t", "\\t");
//        result = result.replace("\\\\b", "\\b");
//        result = result.replace("\\\\n", "\\n");
//        result = result.replace("\\\\r", "\\r");
//        result = result.replace("\\\\f", "\\f");
//        result = result.replace("\\\\'", "\\'");
//        result = result.replace("\\\\\"", "\\\"");

      //  return escapedString;
        return "";
    }

    public static String decodeInputString(String text) {
       // return StringEscapeUtils.unescapeJava(text);
        return "";
    }

    //used if decodeInputString not worked
    public static String decodeInputString2(String text) {
       // String unescapedString = StringEscapeUtils.unescapeJava(text);

        //comment it on 6/3/18 to make sure both ios platform and web platform work well
//        String result = StringEscapeUtils.unescapeJava(unescapedString);

       // return unescapedString;
        return "";
    }

    public static void countdownAdvTimer(final String fragmentName) {

//        //15mins = 900000milliseconds
        advTimer = new CountDownTimer(900000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("commFrag", "seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.d("commFrag", "done!");
                isTimerRunning = false;
                callAdvertisement = allowAdvertise(fragmentName, "all", isTimerRunning);
            }
        }.start();
    }

    public static void getAdvDataTimer(final AsyncTask task, final iAdWebService adWebService, final Activity activity, final String displayType, final String fragmentName) {

//        //15mins = 900000milliseconds
        advDataTimer = new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("commFrag", "seconds remaininggg: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.d("commFrag", "Task status: " + task.getStatus());
                if (task.getStatus() != AsyncTask.Status.FINISHED) {
                    Log.d("commFrag", "Task is finished.");
                    advDataTimer.start();
                } else {
                    //String displayLink = adWebService.getDisplayLink();
                    //String advType = adWebService.getAdvType();

                    if (displayType.equalsIgnoreCase("POPUPFULLSCREEN")) {
                       // intentViewActivity(activity, displayLink, advType, fragmentName);
                    }
                }
            }
        }.start();

    }

    public static void intentViewActivity(Activity activity, String link, String type, String frag) {
        /*Intent intent = new Intent(activity, ViewAdvertisementActivity.class);
        intent.putExtra("advertisement_link", link);
        intent.putExtra("advertisement_type", type);
        intent.putExtra("advertisement_frag", frag);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fadein2, R.anim.fadein2);*/
    }

    public static boolean allowAdvertise(String fragmentName, String updateType, boolean timerIsRunning) {

        boolean result = false, found = false;
        int pos = 0;

        Log.d("commFrag", "fragmentName: " + fragmentName);
        Log.d("commFrag", "allowadvlist: " + allowAdvertiseList);

        if (updateType.equalsIgnoreCase("single")) {
            if (allowAdvertiseList.size() > 0) {
                for (int i = 0; i < allowAdvertiseList.size(); i++) {
                    if (allowAdvertiseList.get(i).get("fragmentName").equalsIgnoreCase(fragmentName)) {
                        found = true;
                        pos = i;
                    }
                }

                if (found) {
                    if (timerIsRunning) {
                        allowAdvertiseList.get(pos).put("allowAdvertise", "false");
                        result = false;
                    } else {
                        allowAdvertiseList.get(pos).put("allowAdvertise", "true");
                        result = true;
                    }
                } else {
                    HashMap<String, String> hm_adv = new HashMap<>();
                    hm_adv.put("fragmentName", fragmentName);
                    hm_adv.put("allowAdvertise", "true");
                    hm_adv.put("isClosed", "false");

                    allowAdvertiseList.add(hm_adv);
                    result = true;
                }
            } else {
                HashMap<String, String> hm_adv = new HashMap<>();
                hm_adv.put("fragmentName", fragmentName);
                hm_adv.put("allowAdvertise", "true");
                hm_adv.put("isClosed", "false");

                allowAdvertiseList.add(hm_adv);
                result = true;
            }
        } else {
            for (int i = 0; i < allowAdvertiseList.size(); i++) {
                allowAdvertiseList.get(i).put("allowAdvertise", "true");
                allowAdvertiseList.get(i).put("isClosed", "false");
            }
            result = true;
        }
        Log.d("commFrag", "2allowadvlist: " + allowAdvertiseList);

        return result;
    }

    public static boolean isAdvertisementClosed(String fragmentName) {
        boolean result = false;
        for (int i = 0; i < allowAdvertiseList.size(); i++) {
            if (allowAdvertiseList.get(i).get("fragmentName").equalsIgnoreCase(fragmentName)) {
                if (allowAdvertiseList.get(i).get("isClosed").equalsIgnoreCase("true")) {
                    result = true;
                }
            }
        }

        return result;
    }

    public static void setCloseAdvertisement(String fragmentName) {
        for (int i = 0; i < allowAdvertiseList.size(); i++) {
            if (allowAdvertiseList.get(i).get("fragmentName").equalsIgnoreCase(fragmentName)) {
                allowAdvertiseList.get(i).put("isClosed", "true");
            }
        }
    }

    //get current time in String dd MMM yyyy, hh:mm a
    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
        String formattedDate = "";

        Date currentDate = new Date();

        try {
            formattedDate = sdf.format(currentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (formattedDate.contains("am")) {
            formattedDate = formattedDate.replace("am", "AM");
        }

        if (formattedDate.contains("pm")) {
            formattedDate = formattedDate.replace("pm", "PM");
        }

        return formattedDate;
    }

    //check if the width is larger than height, then rotate the bitmap by 90 degree
    public static Bitmap checkBitmapRotation(Bitmap bmp) {
        Bitmap bmpFinal = bmp;

        if (bmp.getWidth() > bmp.getHeight()) {
            //fix rotation problem
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bmpFinal = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        }

        return bmpFinal;
    }

    public static void showAdminAccessDialog(final Activity activity) {
        // get prompts.xml view
        final android.app.AlertDialog ShowAlertDialog;
        LayoutInflater li = LayoutInflater.from(activity);
        //View promptsView = li.inflate(R.layout.dialog_common, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set prompts.xml to alertdialog builder
        //alertDialogBuilder.setView(promptsView);

        alertDialogBuilder.setCancelable(false);

        //final LinearLayout llRoot = (LinearLayout) promptsView.findViewById(R.id.layout_root);

        //final Button btnOk = (Button) promptsView.findViewById(R.id.btn_ok);

        //final TextView txtMsg = (TextView) promptsView.findViewById(R.id.tv_msg);

        //final ImageView imgPhoto = (ImageView) promptsView.findViewById(R.id.img_icon);

        //rounded corner dialog for LOLLIPOP and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //llRoot.setBackgroundResource(R.drawable.common_dialog);
            //btnOk.setBackgroundResource(R.drawable.common_dialog_button);
        }

        try {
            //imgPhoto.setImageResource(R.drawable.icon_alert);
           // txtMsg.setText(activity.getResources().getString(R.string.txt_admin_not_allow_access_msg));

        } catch (Exception e) {
            Log.e("commfrag", "" + e.getMessage());
        }

        // create alert dialog
        ShowAlertDialog = alertDialogBuilder.create();
        ShowAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // show it
        ShowAlertDialog.show();

        /*btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAlertDialog.dismiss();
            }
        });*/
    }


    public static String getUnigueId()
    {
        String m_szDevIDShort = "88" +
                (Build.BOARD.length() % 10)
                + (Build.BRAND.length() % 10)
                + (Build.DEVICE.length() % 10)
                + (Build.MANUFACTURER.length() % 10)
                + (Build.MODEL.length() % 10)
                + (Build.PRODUCT.length() % 10);

        String serial = null;
        try
        {
            serial = android.os.Build.class.getField("SERIAL").get(null).toString();

            // Go ahead and return the serial for api => 9
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        }
        catch (Exception e)
        {
            // String needs to be initialized
            serial = "serial"; // some value
        }

        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }
}