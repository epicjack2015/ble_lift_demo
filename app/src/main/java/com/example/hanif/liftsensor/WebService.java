package com.example.hanif.liftsensor;

/**
 * Created by jack on 24-Aug-15.
 */

import android.preference.PreferenceActivity;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

import static com.example.hanif.liftsensor.CommonUtilities.HOST;


public class WebService {
    private static final String TAG = "WebService";

    HashMap<String, Object> result = new HashMap<>();
    HashMap<String, String> data = new HashMap<>();
    String caller = "";
    String sAction = "";
    String sRequestType = "GET";
    final int DEFAULT_TIMEOUT = 120 * 1000; //UPDATED FROM 20sec TO 2min COZ SENDPANICALERT IN PRODUCTION TAKES ABOUT 40sec


    public HashMap<String, Object> invokeWS(final RequestParams params) {

        result.put("success", "false");
        result.put("data", null);
        result.put("error", "");

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        caller = extractDirectFileName(extractSimpleClassName(String.valueOf(stackTraceElements[3].getClassName())));

        try {
            String[] rows = String.valueOf(params).split("&");
            String[][] mtxParams = new String[rows.length][];
            int r = 0;
            for (String row : rows) {
                mtxParams[r++] = row.split("=");
            }
            for (String[] mtxParam : mtxParams) {
                if (mtxParam[0].equals("sRequestType")) {
                    sRequestType = mtxParam[1];
                }

                if (mtxParam[0].equals("sAction")) {
                    sAction = mtxParam[1];
                }
            }
            if (CommonUtilities.DEBUG) {
                Log.d(TAG, Arrays.deepToString(mtxParams));
            }
        } catch (Exception e) {
            Log.e(TAG, "error:" + e);
        }


        // Make RESTful webservice call using AsyncHttpClient object
        SyncHttpClient client = new SyncHttpClient();
        //THIS WILL SET TIMEOUT LONGER INSTEAD OF 10 SECOND TO PREVENT SocketTimeoutException
        client.setTimeout(DEFAULT_TIMEOUT);

        String link = HOST + "/webService/webservice.php?" + params;
        Log.d(TAG, link);

        if (sRequestType.equals("GET")) {
            client.get(HOST + "/webService/webservice.php", params,
                    new AsyncHttpResponseHandler() {


                        @Override
                        public void onSuccess(int i, Header[] headers, byte[] bytes) {

                            try {
                                String response = new String(bytes, "UTF-8");
                                // Extract JSON Object from JSON returned by REST WS
                                JSONObject obj = new JSONObject(String.valueOf(response));
                                // When the JSON response has status boolean value set to true
                                Log.i(TAG, "caller:" + caller);
                                if (obj.has("message")) {
                                    if (obj.getString("message").equals("Success")) {
                                        result.put("success", "true");
                                        switch (sAction) {
                                            case "Login":
                                                data.put("token", obj.getString("token"));
                                                data.put("userid", obj.getString("userid"));
                                                data.put("companyid", obj.getString("companyid"));
                                                data.put("companytype", obj.getString("companytype"));
                                                data.put("companypropertytype", obj.getString("companypropertytype"));
                                                data.put("usertype", obj.getString("usertype"));
                                                data.put("username", obj.getString("username"));
                                                data.put("usergender", obj.getString("usergender"));
                                                data.put("companyname", obj.getString("companyname"));
                                                data.put("condoid", obj.getString("condoid"));
                                                data.put("companylogo", obj.getString("companylogo"));
                                                data.put("userphoto", obj.getString("userphoto"));
                                                data.put("language", obj.getString("language"));
                                                data.put("profilenumber", obj.getString("profilenumber"));
                                                result.put("data", data);
                                                break;
                                            case "wsLogin":
                                                data.put("token", obj.getString("token"));
                                                result.put("data", data);
                                                break;
                                            case "setActiveEmergencyContacts":
                                            case "updtUser":
                                            case "updtCompany":
                                            case "updtUnitStatus":
                                            case "updtVisitorStatus":
                                            case "updtSecurityCompanyInfo":
                                            case "sendEmailVisitorOneTime":
                                            case "sendEmailVisitorMultiple":
                                            case "sendEmailCheckInOut":
                                            case "sendEmailPreRegRegular":
                                            case "sendEmailVisitorRegular":
                                            case "sendEmailPreRegMultiple":
                                            case "sendEmailPreRegOneTime":
                                            case "createRegularVisitLog":
                                            case "addPreVisitor":
                                            case "addResident":
                                            case "addWalkInVisitor":
                                            case "deleteResident":
                                            case "deletePendingResident":
                                            case "addDropOffVisitor":
                                            case "addExtraField":
                                            case "addVisitorPhoto":
                                            case "updtPushNotification":
                                            case "updtPersonalNotes":
                                            case "sendACSPushNotification":
                                            case "sendPushNotification":
                                            case "sendInvitationtoVisitor":
                                            case "stopPanicAlert":
                                            case "updtAndCheckinVisitor":
                                            case "checkinVisitor":
                                            case "deleteEmergencyLink":
                                            case "updtSecurityGuard":
                                            case "approveRejectVisitor":
                                            case "signupNeighbourhood":
                                            case "sendBuzzToManagement":
                                            case "updtGPSLocation":
                                            case "updtNotificationStatus":
                                            case "resetPassword":
                                            case "Logout":
                                                result.put("data", obj.getString("data"));
                                                break;
                                            case "logout":
                                                result.put("data", "Success");
                                                break;
                                            default:
                                                result.put("data", String.valueOf(obj.getJSONArray("data")));
                                                break;
                                        }
                                    }
                                    // Else display error message set in JSON response
                                    else {
                                        result.put("success", "false");
                                        result.put("error", obj.getString("message"));
                                        Log.e(TAG, "JSON response:" + result.get("error"));
                                    }
                                } else {
                                    //NEW WS CHANGE TO status
                                    if (obj.getString("status").equals("Success")) {
                                        result.put("success", "true");
                                        switch (sAction) {
                                            case "addBLE2Logs":
                                            case "completeSyncBLE2":
                                            case "syncBLE2UHFNo":
                                            case "addMyFavouriteList":
                                            case "editMyFavouriteList":
                                            case "updtMyFavouriteList":
                                            case "updtMyVehicle":
                                            case "setSelectedLogin":
                                            case "deleteUnitContact":
                                            case "addUnitContact":
                                            case "acknowledgePanicIncident":
                                            case "updtAnnouncementStatus2":
                                            case "deleteEventAttachment":
                                            case "updtPushNotification":
                                            case "validateActivationCode":
                                            case "getPendingUnitOwner":
                                            case "Logout":
                                                result.put("data", obj.getString("data"));
                                                break;
                                            default:
                                                result.put("data", String.valueOf(obj.getJSONArray("data")));
                                                break;
                                        }
                                    }   // Else display error message set in JSON response
                                    else {
                                        result.put("success", "false");
                                        result.put("error", obj.getString("data"));
                                        Log.e(TAG, "JSON response:" + result.get("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                result.put("error", "Error Occurred while parsing [Server's JSON response might be invalid]! e:" + e);
                                Log.e(TAG, result.get("error") + "");
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {

                            // When Http response code is '404'
                            if (statusCode == 404) {
                                result.put("error", "Requested resource not found");
                            }
                            // When Http response code is '500'
                            else if (statusCode == 500) {
                                result.put("error", "Something went wrong at server end");
                            }
                            // When Http response code other than 404, 500
                            else {
                                result.put("error", "Error(" + String.valueOf(statusCode) + ") : Unexpected Error occurred! [Most common Error: Device might " +
                                        "not be connected to Internet or remote server is not up and running], check for other errors as well");
                            }
                            Log.e(TAG, result.get("error") + "");
                        }


                    });
        } else {
            client.post(HOST + "/webService/webservice.php", params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onProgress(long bytesWritten, long totalSize) {
                            if (sAction.equals("uploadEmergencyPhotos")) {
                               // pb_upload.setMax((int) totalSize);
                               // pb_upload.setProgress((int) bytesWritten);
                            }
                        }



                        @Override
                        public void onSuccess(int i, Header[] headers, byte[] bytes) {

                            String response = "";
                            try {
                                response = new String(bytes, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            try {
                                // Extract JSON Object from JSON returned by REST WS
                                JSONObject obj = new JSONObject(String.valueOf(response));
                                if (obj.has("message")) {
                                    // When the JSON response has status boolean value set to true
                                    if (obj.getString("message").equals("Success")) {
                                        result.put("success", "true");
                                        switch (sAction) {
                                            case "uploadEmergencyPhotos":
                                            case "activateResidentOwnerAcc":
                                                result.put("data", obj.getString("data"));
                                                break;
                                            default:
                                                result.put("data", String.valueOf(obj.getJSONArray("data")));
                                                break;
                                        }
                                    }
                                    // Else display error message set in JSON response
                                    else {
                                        result.put("success", "false");
                                        result.put("error", obj.getString("message"));
                                        Log.e(TAG, "JSON response:" + result.get("error"));
                                    }
                                } else {
                                    // When the JSON response has status boolean value set to true
                                    if (obj.getString("status").equals("Success")) {
                                        result.put("success", "true");
                                        switch (sAction) {
                                            case "addVisitor3":
                                            case "addEventAttachment":
                                            case "uploadEmergencyPhotos":
                                            case "addReportAttachment":
                                            case "activateResidentOwnerAcc":
                                                result.put("data", obj.getString("data"));
                                                break;
                                            default:
                                                result.put("data", String.valueOf(obj.getJSONArray("data")));
                                                break;
                                        }
                                    }
                                    // Else display error message set in JSON response
                                    else {
                                        result.put("success", "false");
                                        result.put("error", obj.getString("status"));
                                        Log.e(TAG, "JSON response:" + result.get("error"));
                                    }
                                }

                            } catch (JSONException e) {
                                result.put("error", "Error Occurred while parsing [Server's JSON response might be invalid]! e:" + e);
                                Log.e(TAG, result.get("error") + " response=" + response);
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {

                            // When Http response code is '404'
                            if (statusCode == 404) {
                                result.put("error", "Requested resource not found");
                            }
                            // When Http response code is '500'
                            else if (statusCode == 500) {
                                result.put("error", "Something went wrong at server end");
                            }
                            // When Http response code other than 404, 500
                            else {
                                result.put("error", "Error(" + String.valueOf(statusCode) + ") : Unexpected Error occurred! [Most common Error: Device might " +
                                        "not be connected to Internet or remote server is not up and running], check for other errors as well");
                            }
                            Log.e(TAG, result.get("error") + "");
                        }

                    });
        }

        return result;

    }


    private static String extractSimpleClassName(String fullClassName) {
        if ((null == fullClassName) || ("".equals(fullClassName)))
            return "";

        int lastDot = fullClassName.lastIndexOf('.');
        if (0 > lastDot)
            return fullClassName;

        return fullClassName.substring(++lastDot);
    }

    private static String extractDirectFileName(String simpleClassName) {
        if ((null == simpleClassName) || ("".equals(simpleClassName)))
            return "";

        int lastSign = simpleClassName.lastIndexOf('$');
        if (0 > lastSign)
            return simpleClassName;
        return simpleClassName.substring(0, lastSign);
    }
}
