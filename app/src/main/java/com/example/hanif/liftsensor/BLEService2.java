package com.example.hanif.liftsensor;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import java.util.List;
import java.util.UUID;

/**
 * Created by HANIF on 8/9/2017.
 */

public class BLEService2 extends Service{
    private final static String TAG = BLEService2.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;
    private Handler mHandler = null;
    public boolean serviceStatus = false;
    private static final String DEVICE_NAME = "1D1D15F48134";
    private BluetoothDevice mDevice;
    private ScanCallback mScanCallback;
    private BluetoothLeScanner mBLEScanner;
    private BluetoothAdapter.LeScanCallback mLeScanCallback;
    private ScanSettings settings;
    private List<ScanFilter> filters;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.hanif.liftsensor.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.hanif.liftsensor.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_CONNECTING =
            "com.example.hanif.liftsensor.ACTION_GATT_CONNECTING";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.hanif.liftsensor.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.hanif.liftsensor.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.hanif.liftsensor.EXTRA_DATA";


    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;

            if (newState == BluetoothProfile.STATE_CONNECTED) {

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    intentAction = ACTION_GATT_CONNECTED;
                    mConnectionState = STATE_CONNECTED;
                    broadcastUpdate(intentAction);
                    //Log.i(TAG, "Connected to GATT server.");
                    // Attempts to discover services after successful connection.
                    //Log.i(TAG, "Attempting to start service discovery:" +
                    //        mBluetoothGatt.discoverServices());
                    mBluetoothGatt.discoverServices();
                } else {
                    //Log.i(TAG, "onConnectionStateChange: " + status + " : " + newState);
                    mHandler = new Handler(Looper.getMainLooper());
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            connect(mBluetoothDeviceAddress);
                        }
                    }, 1000);
                }

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                //Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                intentAction = ACTION_GATT_CONNECTING;
                mConnectionState = STATE_CONNECTING;
                //Log.i(TAG, "Connecting to GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                //Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "onCharacteristicRead: success" + status);
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                Log.d(TAG, "onCharacteristicWrite: success");
            }
            else {
                Log.d(TAG, "onCharacteristicWrite: failed");
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    /*@Override
    public void onCreate() {
        super.onCreate();
        setLECallbacks();
        mHandler = new Handler();
        mBluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder() //
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) //
                    .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES) //
                    .build();

            ParcelUuid puuid = ParcelUuid.fromString("f05abac0-3936-11e5-87a6-0002a5d5c51b");
            ScanFilter filter = new ScanFilter.Builder()
                    //.setDeviceName(null)
                    //.setManufacturerData((int) 0xcd, new byte[]{ (byte) 0xcd, (byte) 0x00, (byte) 0xfe, (byte) 0x14, (byte) 0xad, (byte) 0x11, (byte) 0xcf, (byte) 0x40, (byte) 0x06, (byte) 0x3f, (byte) 0x11, (byte) 0xe5, (byte) 0xbe, (byte) 0x3e, (byte) 0x00, (byte) 0x02, (byte) 0xa5, (byte) 0xd5, (byte) 0xc5, (byte) 0x1b })
                    .setManufacturerData((int) 0xcd, new byte[]{-2, 20, -83, 17, -49, 64, 6, 63, 17, -27, -66, 62, 0, 2, -91, -43, -59})    //, 27})
                    //.setServiceUuid(ParcelUuid.fromString("f05abac0-3936-11e5-87a6-0002a5d5c51b")) //"f05abac1-3936-11e5-87a6-0002a5d5c51b")) //("1bc5d5a5-0200-a687-e511-3639c0ba5af0")) //

                    .build();

            filters = new ArrayList<ScanFilter>();
            filters.add(filter);
        }
        Thread discoverDevices = new Thread(mStartRunnable);
        discoverDevices.setPriority(discoverDevices.MAX_PRIORITY);
        discoverDevices.start();
    }

    private Runnable mStartRunnable = new Runnable() {
        @Override
        public void run() {
            startScan();
        }
    };

    private void startScan() {
        if (mConnectionState == STATE_DISCONNECTED) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                //handle null pinter exception for API >= 21
                if(mBLEScanner != null)
                    mBLEScanner.startScan(filters, settings, mScanCallback);
            }
            mHandler.postDelayed(mStopRunnable, 2500);
        }
    }

    private Runnable mStopRunnable = new Runnable() {
        @Override
        public void run() {
            stopScan();
        }
    };

    private void stopScan() {
        if (mBluetoothAdapter.isEnabled()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                //handle null pinter exception for API >= 21
                if(mBLEScanner != null)
                    mBLEScanner.stopScan(mScanCallback);
            }
        }
    }*/

    private void setLECallbacks() {
        /*
         * We are looking for SensorTag devices only, so validate the name that
         * each device reports before adding it to our collection
         */
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            // Device scan callback.
            mLeScanCallback =
                    new BluetoothAdapter.LeScanCallback() {

                        @Override
                        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                            if (DEVICE_NAME.equals(device.getName())) {
                                mDevice = device;
                                mBluetoothDeviceAddress = mDevice.getAddress();
                                connect(mBluetoothDeviceAddress);
                                mConnectionState = STATE_CONNECTING;
                                if(device.getBondState() == BluetoothDevice.BOND_BONDED) {

                                } else if (device.getBondState() == BluetoothDevice.BOND_BONDING) {

                                } else if(device.getBondState() == BluetoothDevice.BOND_NONE) {
                                    connect(device.getAddress());
                                }
                            }
                        }
                    };

        } else {
            mScanCallback = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, final ScanResult result) {
                    if (DEVICE_NAME.equals(result.getDevice().getName())) {
                        mDevice = result.getDevice();
                        mBluetoothDeviceAddress = mDevice.getAddress();
                        connect(mBluetoothDeviceAddress);
                        mConnectionState = STATE_CONNECTING;
                        if(mDevice.getBondState() == BluetoothDevice.BOND_BONDED) {

                        } else if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {

                        } else if(mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                            connect(mDevice.getAddress());
                        }
                    }
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    //for (ScanResult sr : results) {
                    //    Log.i("Scan Result", sr.toString());
                    //}
                    //super.onBatchScanResults(results);
                }

                @Override
                public void onScanFailed(int errorCode) {
                    //Log.i("Scan Failed", "Code:" + errorCode);
                    //super.onScanFailed(errorCode);
                }
            };
        }
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                stringBuilder.append(characteristic.getUuid().toString() + ":");
                for(byte byteChar : data)
                    stringBuilder.append(String.format("%02X", byteChar));

                intent.putExtra(EXTRA_DATA, stringBuilder.toString());
            }
        }
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        BLEService2 getService() {
            return BLEService2.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        serviceStatus = true; return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        if (mHandler != null) {
            mHandler.removeCallbacks(null);
        }

        serviceStatus = false;
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                //Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            //Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            //Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        mGattCallback.onConnectionStateChange(mBluetoothGatt, BluetoothGatt.GATT_SUCCESS, BluetoothProfile.STATE_CONNECTING);   //JAY

        // Previously connected device.  Try to reconnect.

        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            //Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }


        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            //Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to auto connect to the device, so we are setting the autoConnect
        // parameter to true.
        //mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        (new Handler(Looper.getMainLooper())).post(new Runnable() {
            @Override
            public void run() {
                mBluetoothGatt = device.connectGatt(BLEService2.this, true, mGattCallback);
            }
        });

        //try {
        //    mBluetoothGatt = (BluetoothGatt)((device.getClass().getMethod("connectGatt", Context.class, BluetoothGattCallback.class, int.class)).invoke(device, BLEService2.this, false, mGattCallback, BluetoothDevice.TRANSPORT_LE));
        //} catch (Exception e) {}//JAY


        //Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            //Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    public String getDeviceAddress() {
        return mBluetoothDeviceAddress;
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            //Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicWrite(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] value) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            //Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        characteristic.setValue(value);
        mBluetoothGatt.writeCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            //Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                UUID.fromString(BLEGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        if (enabled) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        } else {
            descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        }
        mBluetoothGatt.writeDescriptor(descriptor);

    }

    /**
     * Enables or disables indication on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicIndication(BluetoothGattCharacteristic characteristic,
                                            boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            //Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                UUID.fromString(BLEGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        if (enabled) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
        } else {
            descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        }
        mBluetoothGatt.writeDescriptor(descriptor);

    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }
}
