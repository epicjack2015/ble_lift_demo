package com.example.hanif.liftsensor;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;


import com.example.hanif.liftsensor.CommonUtilities;
import com.example.hanif.liftsensor.WebService;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


import static com.example.hanif.liftsensor.CommonUtilities.IADS;

/**
 * Created by admin on 12/8/2017.
 */

public class iAdWebService {
    private static final String TAG = "iAdWebService";
    private Activity activity;
    private String mAction = "getIadv";
    RequestParams parameters = new RequestParams();
    HashMap<String, Object> result;
    Boolean status;
    JSONArray data;
    public static getIadvTask advTask;

    private String token, cloudCompanyId, displayType;
    private String advId, displayLink, advType;


    public iAdWebService(Activity activity, String token, String cloudCompanyId, String displayType) {
        this.activity = activity;
        this.token = token;
        this.cloudCompanyId = cloudCompanyId;
        this.displayType = displayType;

        getIadvTask();
    }

    public void getIadvTask() {
        if (CommonUtilities.isConnectionAvailable(activity)) {
            if (CommonUtilities.pingHost()) {
                advTask = new getIadvTask(mAction, token, displayType);
                advTask.execute();
                CommonUtilities.stoprunning = false;
            } else {
                CommonUtilities.dismissProgress();
                //String message = activity.getResources().getString(R.string.lost_connection_to_host);
                //CommonUtilities.showAlertDialog(activity, message, false);
            }
        } else {
            CommonUtilities.dismissProgress();
            //String message = activity.getResources().getString(R.string.no_internet_connection);
            //CommonUtilities.showAlertDialog(activity, message, false); // if dont want close put 'false'
        }
    }

    public class getIadvTask extends AsyncTask<Void, Void, Void> {

        WebService ws = new WebService();
        private final String sAction;
        private final String sToken;
        private final String sType;


        getIadvTask(String mAction, String mToken, String mType) {
            sAction = mAction;
            sToken = mToken;
            sType = mType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            CommonUtilities.showProgress(activity, activity.getResources().getString(R.string.loading_progress));
        }

        @Override
        protected Void doInBackground(Void... params) {
            parameters.put("sAction", sAction);
            parameters.put("sToken", sToken);
            parameters.put("sType", sType);

            // Invoke REST Web service to check visitor
            result = ws.invokeWS(parameters);

            status = Boolean.parseBoolean(result.get("success").toString());

            Log.d("RESPONSE", String.valueOf(result));
            if (status) {
                try {

                    if (sAction == mAction) {
                        data = new JSONArray(result.get("data").toString());
                        JSONObject iadv_obj = data.getJSONObject(0);

                        advId = iadv_obj.getString("advId");
                        advType = iadv_obj.getString("advType");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Fail to catch json data. ");
                }
            } else {
                Log.e(TAG, "Couldn't get any data from the url");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
//            CommonUtilities.dismissProgress();
            if (status) {
                displayLink = IADS + "/app/ad.php?iCompany=" + cloudCompanyId + "&iAdId=" + advId;
                Log.d("frag", "diplaylink: " + displayLink + " advtype: " + advType);
            }
        }

    }//END getIadvTask

    public String getDisplayLink() {
        return displayLink;
    }

    public String getAdvType() {
        return advType;
    }
}
