package com.example.hanif.liftsensor;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;



public class DeviceControlActivity extends Activity {
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String EXTRAS_DEVICE_REAL_ADDRESS = "DEVICE_REAL_ADDRESS";
    public static final String EXTRAS_SCAN_RESULT = "SCAN_RESULT";
    private static final String TAG = "DeviceControlActivity";

    static {
        System.loadLibrary("native.lob");
    }
    public native void initialize();
    public native void generateSeed(long seed1,long seed2, long seed3, long seed4, long seed5, long seed6);
    public native String encryptData(long v0 , long v1);
    public native void decryptData(byte[] b0, byte[] b1);

    RequestParams parameters = new RequestParams();
    HashMap<String, Object> result;
    Boolean status;
    String message;
    JSONObject data;
    private Boolean flag_loading;
    String AES = "AES";
    EditText endeVal;
    String outputText;
    Button btnEncrypt , btnDecrypt;
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private String mAction = "addBLE2Logs";
    private String token = "AZK88KJD182IZKA";

    RelativeLayout llLv;
    ArrayList<String> bleList = new ArrayList<>(10);
    Set<String> hs = new HashSet<>();
    ArrayAdapter<String> arrayAdapter;
    ListView myListView;
    private Button btnShowData;
    String testid;
    String logstr;
    boolean isGone = false;
    //TextView loading;
    ProgressBar progressBar;
    String concater;
    String deviceMacAddress;


    private String mDeviceName;
    private String mDeviceAddress, mDeviceBoardAddress;
    private String mManufactureData;
    private BluetoothAdapter mBluetoothAdapter;
    private ExpandableListView mGattServicesList;
    private BLEService mBleService;
    private List<BluetoothGattCharacteristic> mGattCharacteristics;
    private boolean mConnected = false;
    private boolean mRead = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private ScanResult mScanResult;
    private byte[] mScanResultDep;
    private BluetoothGattCharacteristic ledSensor = null;
    private BluetoothGattCharacteristic ledSensorWrite = null;
    private BluetoothGattCharacteristic ledSensorTest = null;
    private String logGattCharacteristic;
    private Double temperature;
    private Double batteryStatus = -1.0;
    private CountDownTimer timeoutTimer = null;
    private boolean statusTimer = false;
    private byte viewIndex = 0;

    private android.os.Handler mHandler = new android.os.Handler();
    private Runnable mTimer;

    //in Hex
    private String levelControlHead = "A10601";
    private String levelBle2Head = "A16010";
    private String levelControlTail = "01"; //random Hex number between 01 ~ FF
    private String timeControlHead = "A20755";
    private String cardControlHead = "D102";
    private String userControlHead = "A304";
    private String restrictClose = "1E";

    private boolean mBluetoothStatus = false;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_ENABLE_LOCATION = 2;
    private static final long SCAN_PERIOD = 10000;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    //buttons
    private Button btnLv1, btnLv2, btnLv3, btnLv4, btnLv5, btnLv6, btnLv7, btnLv8;
    private Button btnLv9, btnLv10, btnLv11, btnLv12, btnLv13, btnLv14, btnLv15, btnLv16;
    private Button btnSyncDate, btnReadValue, btnCard1, btnCard2, btnCard3, btnCard4;
    private Button btnAddUser, btnDeleteUser, btnDeleteAllUsers;

    private LinearLayout llControl, llLiftControl, llCardControl, llDefault;
    private ScrollView svControl;
    private EditText edtWrittenValue, edtReadValue;
    private TextView tvDefault;
    private TextView useTv;

    private AlertDialog ShowAlertDialog;

    private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                    case BluetoothAdapter.STATE_OFF:
                        //case BluetoothAdapter.STATE_TURNING_OFF:
                        mBluetoothStatus = true;
                        new AlertDialog.Builder(context)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.error_bluetooth_off)
                                .setIcon(R.mipmap.ic_launcher)
                                .setCancelable(false)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(DeviceControlActivity.this, R.string.error_bluetooth_off, Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                })
                                .show();
                        //finish();
                        break;

                    case BluetoothAdapter.STATE_ON:
                        //case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBleService = ((BLEService.LocalBinder) service).getService();
            if (!mBleService.initialize()) {
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBleService.connect(mDeviceAddress);

            timeoutTimer = new CountDownTimer(60000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    statusTimer = true;
                }

                @Override
                public void onFinish() {
                    statusTimer = false;
                    Toast.makeText(DeviceControlActivity.this, R.string.ble_connect_error, Toast.LENGTH_SHORT).show();
                    finish();
                }
            };
            timeoutTimer.start();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBleService = null;
        }
    };

    @Override
    public void onBackPressed() {


        if (svControl.getVisibility() == View.GONE && llLiftControl.getVisibility() == View.GONE || bleList.size() >1) {
            llLv.setVisibility(View.GONE);
            llLiftControl.setVisibility(View.VISIBLE);
            svControl.setVisibility(View.VISIBLE);
        }

        if (!isGone){
            Intent backtoScanIntent = new Intent(DeviceControlActivity.this , DeviceScanActivity.class);
            startActivity(backtoScanIntent);
            finish();
        }
        isGone = false;

    }

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BLEService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                invalidateOptionsMenu();
            } else if (BLEService.ACTION_GATT_DISCONNECTED.equals(action)) {
                if (!statusTimer) {
                    if (timeoutTimer != null) {
                        timeoutTimer.cancel();
                    }
                    mConnected = false;
                    invalidateOptionsMenu();
                    finish();
                } else {
                    if (mBleService != null) {
                        mBleService.connect(mDeviceAddress);
                    }
                }

            } else if (BLEService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                if (timeoutTimer != null) {
                    timeoutTimer.cancel();
                }
                //displayGattServices(mBleService.getSupportedGattServices());
                for (BluetoothGattService gattService : mBleService.getSupportedGattServices()) {
                    //Log.i("Service", gattService.getUuid().toString());
                    if (gattService.getUuid().toString().equals("ad11cf40-063f-11e5-be3e-0002a5d5c51b")) {
                        //for BLE lift
                        mGattCharacteristics = gattService.getCharacteristics();
                        for (final BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics()) {

                            //Write to characteristic number 3 first before subscribing to characteristic number 1.
                            if (gattCharacteristic.getUuid().toString().equals("bf3fbd80-063f-11e5-9e69-0002a5d5c503")) {
                                Log.d(TAG, "returnsensorfound: ");
                                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:02")) {
                                    ledSensor = gattCharacteristic;
                                    ledSensorTest = gattCharacteristic;
                                    logGattCharacteristic = gattCharacteristic.getUuid().toString();
                                }

                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d(TAG, "returnsensorfound and got: ");
                                         ledSensorWrite = gattCharacteristic;
                                    }
                                }, 500);
                                //ledSensorWrite = gattCharacteristic;
                                final String writeCode = "A50401";
                                String stopCode = "A50400";
                                Log.d(TAG, "In and got sensor");
                                if (!mDeviceBoardAddress.equals("34:81:F4:15:1D:02") && !mDeviceBoardAddress.equals("34:81:f4:15:1D:07")
                                        && !mDeviceBoardAddress.equals("34:81:F4:15:1D:09") && !mDeviceBoardAddress.equals("34:81:F4:15:1D:37")
                                        && !mDeviceBoardAddress.equals("34:81:F4:15:1D:28")) {
                                //NO NEED TO SEND BROADCAST WHEN SENDING COMMAND
                                  /*
                                    mHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mBleService.writeCharacteristic(ledSensorWrite, hexStringToByteArray(writeCode));
                                        }
                                    }, 1000);
                                    */
                                } else {
                                    ledSensor = gattCharacteristic;
                                        final int charaProp = ledSensor.getProperties();
                                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                                            // If there is an active notification on a characteristic, clear
                                            // it first so it doesn't update the data field on the user interface.
                                            if (mNotifyCharacteristic != null) {

                                                mBleService.setCharacteristicNotification(
                                                        mNotifyCharacteristic, false);
                                                mNotifyCharacteristic = null;
                                            }

                                            if (ledSensor != null && ledSensor.getUuid().toString().equals(logGattCharacteristic)) {
                                                mHandler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mBleService.readCharacteristic(ledSensor);
                                                        Log.d(TAG, "read is done");
                                                    }
                                                }, 1000);

                                            } else {
                                                tvDefault.setText("Writing to BLE Controller...");
                                            }
                                        }
                                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                                            mNotifyCharacteristic = ledSensor;
                                            if (bleList.size() > 15) {
                                                mBleService.setCharacteristicNotification(
                                                        ledSensor, false);
                                            } else {
                                                mBleService.setCharacteristicNotification(
                                                        ledSensor, true);
                                            }
                                        }
                                }
                                Log.d(TAG, "In and got sensor and written");
                            }

                            //Subscribe to characteristic number 1 to read logs after writing to char 3.
                            if (gattCharacteristic.getUuid().toString().equals("bf3fbd80-063f-11e5-9e69-0002a5d5c501")) {
                                if (!mDeviceBoardAddress.equals("34:81:F4:15:1D:02") && !mDeviceBoardAddress.equals("34:81:F4:15:1D:07")
                                        && !mDeviceBoardAddress.equals("34:81:F4:15:1D:09") && !mDeviceBoardAddress.equals("34:81:F4:15:1D:37")
                                        && !mDeviceBoardAddress.equals("34:81:F4:15:1D:28")) {
                                ledSensor = gattCharacteristic;
                                    final int charaProp = ledSensor.getProperties();
                                    if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                                        // If there is an active notification on a characteristic, clear
                                        // it first so it doesn't update the data field on the user interface.
                                        if (mNotifyCharacteristic != null) {

                                            mBleService.setCharacteristicNotification(
                                                    mNotifyCharacteristic, false);
                                            mNotifyCharacteristic = null;
                                        }

                                        if (ledSensorWrite != null) {
                                            mHandler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Log.d(TAG, "characteristic read");
                                                    mBleService.readCharacteristic(ledSensor);
                                                }
                                            }, 1000);

                                        } else {
                                            tvDefault.setText("Writing to BLE Controller...");
                                        }



                                    }
                                    if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                                        mNotifyCharacteristic = ledSensor;
                                        if (bleList.size() > 15) {
                                            mBleService.setCharacteristicNotification(
                                                    ledSensor, false);
                                        } else {
                                            mBleService.setCharacteristicNotification(
                                                    ledSensor, true);
                                        }

                                    }
                                }
//                                if (ledSensorWrite != null) {
                                //try fin
                                Toast.makeText(DeviceControlActivity.this, R.string.device_connected, Toast.LENGTH_SHORT).show();

                                llDefault.setVisibility(View.GONE);
                                svControl.setVisibility(View.VISIBLE);
                                //llLv.setVisibility(View.VISIBLE);
                                llLiftControl.setVisibility(View.VISIBLE);

                                initialize();

                                //String blMacAddress = "34:81:F4:15:24:21";
                                String blMacAddress = mDeviceAddress.toUpperCase();

                                Log.d(TAG, "returnMacAd: " + blMacAddress);

                                String [] blMacAddressAr = blMacAddress.split(":");
                                int lengthCount = blMacAddressAr.length;
                                long []byte_raw_data = new long[lengthCount];


                                for (int i = 0; i < byte_raw_data.length; i++){
                                    Log.d(TAG, "returnArray12: " + blMacAddressAr[i]);

                                    byte_raw_data[i] = Long.parseLong(blMacAddressAr[i], 16);
                                    //byte_raw_data[0] = 0x | 40;

                                }
                                Log.d(TAG, "returnArray1: " + Arrays.toString(byte_raw_data));
                                generateSeed(byte_raw_data[0],byte_raw_data[1],byte_raw_data[2]
                                        ,byte_raw_data[3],byte_raw_data[4],byte_raw_data[5]);

                                Toast.makeText(DeviceControlActivity.this, "Generated seed: " + blMacAddress + " with byte array of : " + Arrays.toString(byte_raw_data), Toast.LENGTH_LONG).show();

                               /* }
                                else {
                                    tvDefault.setText("Writing to BLE Controller..");
                                }*/
                            }
                        }

                    } else if (gattService.getUuid().toString().equals("0000fee9-0000-1000-8000-00805f9b34fb")) {
                        //for BLE card (remote control)
                        mGattCharacteristics = gattService.getCharacteristics();
                        for (BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics()) {
                            if (gattCharacteristic.getUuid().toString().equals("d44bc439-abfd-45a2-b575-925416129600")) {
                                ledSensor = gattCharacteristic;
                                Toast.makeText(DeviceControlActivity.this, R.string.device_connected, Toast.LENGTH_SHORT).show();
                                setOnClick();
                                llDefault.setVisibility(View.GONE);
                                llCardControl.setVisibility(View.VISIBLE);

                            }
                        }
                    } else if (gattService.getUuid().toString().equals("f05abac1-3936-11e5-87a6-0002a5d5c51b")) {
                        for (BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics()) {
                            if (((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) || ((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0)) {
                                mBleService.setCharacteristicNotification(gattCharacteristic, true);

                                try {
                                    Thread.sleep(350);
                                } catch (InterruptedException e) {
                                }

                            }
                        }

                    }
                }

                if (mRead) {

                    mTimer = new Runnable() {
                        @Override
                        public void run() {
                            if (mRead) {
                                for (BluetoothGattCharacteristic gattCharacteristic : mGattCharacteristics) {
                                    if ((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                                        mBleService.readCharacteristic(gattCharacteristic);
                                        try {
                                            Thread.sleep(10);
                                        } catch (InterruptedException e) {
                                        }
                                    }
                                }
                                mHandler.postDelayed(this, 0);
                            }
                        }
                    };
                    mHandler.postDelayed(mTimer, 100);
                }

                invalidateOptionsMenu();

            } else if (BLEService.ACTION_DATA_AVAILABLE.equals(action)) {
                //Log.d(TAG, "onReceive: " + ledSensorWrite.getUuid().toString());
                if (!mDeviceBoardAddress.equals("34:81:F4:15:1D:02") && !mDeviceBoardAddress.equals("34:81:F4:15:1D:07")
                        && !mDeviceBoardAddress.equals("34:81:F4:15:1D:09") && !mDeviceBoardAddress.equals("34:81:F4:15:1D:37")
                        && !mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    ledSensor = ledSensorWrite;
                }
                if (ledSensor != null) {

                        addData(intent.getStringExtra(BLEService.EXTRA_DATA));
                        if (bleList.size() > 10) {

                            String num1 = bleList.get(1).trim();
                            String num2 = bleList.get(2).trim();
                            String num3 = bleList.get(3).trim();
                            String num4 = bleList.get(4).trim();
                            String num5 = bleList.get(5).trim();


                            concater = num1.trim() + "," + num2.trim() + "," + num3.trim() + "," + num4.trim() + "," + num5.trim();

                            getBLETransactionLog();
                        }

                }

            }
        }
    };

    private void addData(String data) {
        if (data != null) {

            if (data.contains("bf3fbd80-063f-11e5-9e69-0002a5d5c501:")) {
                logstr = data.replace("bf3fbd80-063f-11e5-9e69-0002a5d5c501:", "").trim();
                Log.d(TAG, "addDataResult1: " + logstr);
            }

            if (data.contains("bf3fbd80-063f-11e5-9e69-0002a5d5c503:")) {
                logstr = data.replace("bf3fbd80-063f-11e5-9e69-0002a5d5c503:", "").trim();
                Log.d(TAG, "addDataResult1: " + logstr);
            }

            //logstr = logstr.replaceAll("..(?!$)", "$0 ");
            logstr = logstr.replaceAll("^\\s+|\\s+$", "");
            Log.d(TAG, "addDataResult2: " + logstr);

            bleList.add(0,logstr);
            HashSet hs1 = new HashSet();
            hs1.addAll(bleList);
            bleList.clear();
            bleList.addAll(hs1);
            myListView.setAdapter(arrayAdapter);


            if (bleList.size() > 1 ) {
                arrayAdapter.notifyDataSetChanged();
            }

            for (int i = 0; i < bleList.size(); i++){
                if (bleList.get(i).equalsIgnoreCase("ABCD") || bleList.get(i)
                        .equalsIgnoreCase("AB CD")) {
                    bleList.remove(i);
                }
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "start device control");

        getActionBar().setTitle(R.string.app_name);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

//        mDeviceAddress = "34:81:F4:15:1D:1D";
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mDeviceBoardAddress = intent.getStringExtra("DEVICE_REAL_ADDRESS");
        deviceMacAddress = getMacAddr();
        Log.d(TAG, "device name: " + deviceMacAddress);
        deviceMacAddress = deviceMacAddress.replace(":","");
        Log.d(TAG, "device address: " + deviceMacAddress);
        Log.d(TAG, "device address board: "+  mDeviceBoardAddress);

        setContentView(R.layout.activity_device_control);

        findViewById();
        //setOnClick();

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }*/

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if (mDeviceAddress.equals("34:81:F4:15:1D:37") || mDeviceAddress.equals("08:7C:BE:B2:23:1C") ||
                mDeviceAddress.equals("08:7C:BE:B2:B1:71") || mDeviceAddress.equals("08:7C:BE:B2:B1:6F")) {
            //do nothing
        } else {
            //tvDefault.setText(getResources().getString(R.string.sensor_not_supported));
            invalidateOptionsMenu();
        }

        registerReceiver(bluetoothReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled() && !mBluetoothStatus) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        /*Intent intentService = new Intent(this, BLEService.class);
        startService(intentService);*/

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

        Intent gattServiceIntent = new Intent(this, BLEService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        setOnClick();
        arrayAdapter = new ArrayAdapter<String>(DeviceControlActivity.this, android.R.layout.simple_list_item_1, bleList);
    }

    private void findViewById() {

        btnShowData = (Button) findViewById(R.id.btn_show_data);
        useTv = (TextView) findViewById(R.id.use_tv);
        myListView = (ListView) findViewById(R.id.list_item);
        llLv = (RelativeLayout) findViewById(R.id.ll_lv);
        //loading = (TextView) findViewById(R.id.loading);
        //progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnLv1 = (Button) findViewById(R.id.btn_level_1);
        btnLv2 = (Button) findViewById(R.id.btn_level_2);
        btnLv3 = (Button) findViewById(R.id.btn_level_3);
        btnLv4 = (Button) findViewById(R.id.btn_level_4);
        btnLv5 = (Button) findViewById(R.id.btn_level_5);
        btnLv6 = (Button) findViewById(R.id.btn_level_6);
        btnLv7 = (Button) findViewById(R.id.btn_level_7);
        btnLv8 = (Button) findViewById(R.id.btn_level_8);
        btnLv9 = (Button) findViewById(R.id.btn_level_9);
        btnLv10 = (Button) findViewById(R.id.btn_level_10);
        btnLv11 = (Button) findViewById(R.id.btn_level_11);
        btnLv12 = (Button) findViewById(R.id.btn_level_12);
        btnLv13 = (Button) findViewById(R.id.btn_level_13);
        btnLv14 = (Button) findViewById(R.id.btn_level_14);
        btnLv15 = (Button) findViewById(R.id.btn_level_15);
        btnLv16 = (Button) findViewById(R.id.btn_level_16);
        btnAddUser = (Button) findViewById(R.id.btn_add_user);
        btnDeleteUser = (Button) findViewById(R.id.btn_delete_user);
        btnDeleteAllUsers = (Button) findViewById(R.id.btn_delete_all_user);
        btnSyncDate = (Button) findViewById(R.id.btn_sync_date);
        btnReadValue = (Button) findViewById(R.id.btn_read_value);
        btnCard1 = (Button) findViewById(R.id.btn_card_1);
        btnCard2 = (Button) findViewById(R.id.btn_card_2);
        btnCard3 = (Button) findViewById(R.id.btn_card_3);
        btnCard4 = (Button) findViewById(R.id.btn_card_4);

        llControl = (LinearLayout) findViewById(R.id.ll_control);
        llLiftControl = (LinearLayout) findViewById(R.id.ll_lift_control);
        llDefault = (LinearLayout) findViewById(R.id.ll_default);
        llCardControl = (LinearLayout) findViewById(R.id.ll_card_control);
        svControl = (ScrollView) findViewById(R.id.sv_control);
        edtWrittenValue = (EditText) findViewById(R.id.edt_written_value);
        edtReadValue = (EditText) findViewById(R.id.edt_read_value);
        tvDefault = (TextView) findViewById(R.id.tv_default);

        endeVal = (EditText) findViewById(R.id.edt_encrypt_value);
        btnEncrypt = (Button) findViewById(R.id.btn_encrypt_data);
        btnDecrypt = (Button) findViewById(R.id.btn_decrypt_data);
    }

    public static byte[] fromUnsignedInt(long value)
    {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putLong(value);

        return Arrays.copyOfRange(bytes, 4, 8);
    }

    //test for java
    private String encryptText(String data) throws Exception {
        SecretKeySpec key = generateKey(data);
        Cipher c = Cipher.getInstance(AES);
        c.init(Cipher.ENCRYPT_MODE , key);
        byte[] encVal = c.doFinal(data.getBytes());
        String encryptedValue = Base64.encodeToString(encVal , Base64.DEFAULT);

        return encryptedValue;
    }

    //test for jave encryption
    private SecretKeySpec generateKey(String data) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = data.getBytes("UTF-8");
        digest.update(bytes , 0 , bytes.length);
        byte[] key = digest.digest();
        SecretKeySpec secretKeySpec = new SecretKeySpec(key , "AES");

        return secretKeySpec;
    }
    //test for jave encryption
    private String decryptText(String outputText, String param) throws Exception {
        SecretKeySpec key = generateKey(param);
        Cipher c = Cipher.getInstance(AES);
        c.init(Cipher.DECRYPT_MODE , key);
        byte[] decodeValue = Base64.decode(outputText , Base64.DEFAULT);
        byte[] decValue = c.doFinal(decodeValue);
        String decryptedValue = new String(decValue);

        return decryptedValue;
    }

    private void setOnClick() {
        btnLv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                String param = "30656365"; //hex equivalent to "0ece"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;

                //*************//Henry's encryption method// ********

                /*
                String v0 = "06013065";
                String v1 = "6365011E";
                */

                //Changed of command from 06013065 -> 60103065
                long v0 = 1611673701; // int of 60103065
                long v1 = 1667563806; // int of 6365011E


                Log.d(TAG, "BytesEncrypt11: " + v0);
                Log.d(TAG, "BytesEncrypt12: " + v1);

                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:02")){
                    mBleService.writeCharacteristic(ledSensorTest, hexStringToByteArray(param));
                } else {
                    if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                            || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                        //A10601306563650100
                        //int of 06013065
                        v0 = 100741221;
                        //int of 63650100
                        v1 = 1667563776;
                    }
                    String concatedString = encryptAndWrite(v0, v1);
                    Toast.makeText(DeviceControlActivity.this, "Button 1 is clicked and" +
                                    " the command string to be converted to byte array is: \n" + concatedString,
                            Toast.LENGTH_LONG).show();
                }


                //*******************//Henry's encryption method END// ********


                // A1 06 01 30 65 63 65 01 - 4 bytes -> v0, 4 bytes -> v1
                //vCRYPTO_ACT_Encryption("06 01 30 65","63 65 01 00")  --extra behind add 2bits 00

                // String c1 - v0 - get the data from the encrypted function
                // String c2 - v1

//                mBleService.writeCharacteristic(ledSensor, param.getBytes());

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    outputText = decryptText(outputText , "30656365" + levelControlTail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                endeVal.setText(outputText);
            }
        });

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    outputText = encryptText("30656365" + levelControlTail);
                    endeVal.setText(outputText);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnLv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                String param = "31353832"; //hex equivalent to "1582"
                edtWrittenValue.setText(param + levelControlTail);
                //param = levelBle2Head + param + "02" + restrictClose;
                param = levelControlHead + param + levelControlTail;

                //A1061031353832021E
                //Changed of command from 06013139 -> 60103139
                long v0 = 1611673909; // int of 60103139
                long v1 = 942801438; // int of 6661031E

//                byte[] v0Real = v0.getBytes();
//                byte[] v1Real = v1.getBytes();
                Log.d(TAG, "BytesEncrypt11: " + v0);
                Log.d(TAG, "BytesEncrypt12: " + v1);
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:02")){
                    mBleService.writeCharacteristic(ledSensorTest, hexStringToByteArray(param));
                } else {
                    if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                            || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                        //int of 06013135
                        v0 = 100741429;
                        //int of 38320200
                        v1 = 942801408;
                    }
                    String concatedString = encryptAndWrite(v0, v1);
                    Log.d(TAG, "secondFloorParam: " + param);
                    Toast.makeText(DeviceControlActivity.this, "Button 2 is clicked and" +
                            " the command string to be converted to byte array is: \n" +
                            concatedString, Toast.LENGTH_LONG).show();
                }

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                String param = "31396661"; //hex equivalent to "19fa"
                edtWrittenValue.setText(param + levelControlTail);
                //param = levelBle2Head + param + "03" + restrictClose;
                param = levelControlHead + param + levelControlTail;

                Log.d(TAG, "giveBackParam: " + param);

                //A1601031396661031E
                //Changed of command from 06013065 -> 60103065
                long v0 = 1611673913; // int of 60103135
                long v1 = 1717633822; // int of 3832021E

                Log.d(TAG, "BytesEncrypt11: " + v0);
                Log.d(TAG, "BytesEncrypt12: " + v1);

                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:02")){
                    mBleService.writeCharacteristic(ledSensorTest, hexStringToByteArray(param));
                } else {
                    if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                            || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")){
                        //int of 06013139
                        v0 = 100741433;
                        //int of 66610200
                        v1 = 1717633536;
                    }
                    String concatedString = encryptAndWrite(v0, v1);
                    Log.d(TAG, "secondFloorParam: " + param);
                    Toast.makeText(DeviceControlActivity.this, "Button 3 is clicked and the command string to be converted to byte array is: \n" + concatedString, Toast.LENGTH_LONG).show();

                }

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
                long v0 = 0;
                long v1 = 0;
                String param = "32306165"; //hex equivalent to "20ae"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601323061650100
                    //int of 06013230
                    v0 = 100741680;
                    //int of 61650100
                    v1 = 1634009344;
                }
                String concatedString = encryptAndWrite(v0, v1);



                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "32363434"; //hex equivalent to "2644"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601323634340100
                    //int of 06013236
                    v0 = 100741686;
                    //int of 34340100
                    v1 = 875823360;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "33303532"; //hex equivalent to "3052"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601333035320100
                    //int of 06013330
                    v0 = 100741936;
                    //int of 35320100
                    v1 = 892469504;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "33383234"; //hex equivalent to "3824"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601333832340100
                    //int of 06013338
                    v0 = 100741944;
                    //int of 32340100
                    v1 = 842268928;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
                long v0 = 0;
                long v1 = 0;

                String param = "33393432"; //hex equivalent to "3942"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601333934320100
                    //int of 06013339
                     v0 = 100741945;
                    //int of 34320100
                     v1 = 875692288;
                }
                String concatedString = encryptAndWrite(v0, v1);
//                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:37")){
//                    mBleService.writeCharacteristic(ledSensorTest, hexStringToByteArray(param));
//                } else {
//                    mBleService.writeCharacteristic(ledSensor, hexStringToByteArray(param));
//                }

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
                long v0 = 0;
                long v1 = 0;

                String param = "33666636"; //hex equivalent to "3ff6"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601336666360100
                    //int of 06013366
                    v0 = 100741990;
                    //int of 66360100
                    v1 = 1714815232;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "34366161"; //hex equivalent to "46aa"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601343661610100
                    //int of 06013436
                    v0 = 100742198;
                    //int of 61610100
                    v1 = 1633747200;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "34643565"; //hex equivalent to "4d5e"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601346435650100
                    //int of 06013464
                    v0 = 100742244;
                    //int of 35650100
                    v1 = 895811840;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                String param = "35326634"; //hex equivalent to "52f4"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;

                long v0 = 0;
                long v1 = 0;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601353266340100
                    //int of 06013532
                    v0 = 100742450;
                    //int of 66340100
                    v1 = 1714684160;
                }
                String concatedString = encryptAndWrite(v0, v1);
//                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:37")){
//                    mBleService.writeCharacteristic(ledSensorTest, hexStringToByteArray(param));
//                } else {
//                    mBleService.writeCharacteristic(ledSensor, hexStringToByteArray(param));
//                }

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "35383861"; //hex equivalent to "588a"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601353838610100
                    //int of 06013538
                    v0 = 100742456;
                    //int of 38610100
                    v1 = 945881344;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "36303563"; //hex equivalent to "605c"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601363035630100
                    //int of 06013630
                    v0 = 100742704;
                    //int of 35630100
                    v1 = 895680768;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                long v0 = 0;
                long v1 = 0;
                String param = "36383265"; //hex equivalent to "682e"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601363832650100
                    //int of 06013638
                    v0 = 100742712;
                    //int of 32650100
                    v1 = 845480192;
                }
                String concatedString = encryptAndWrite(v0, v1);

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnLv16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRead) {
                    mHandler.removeCallbacks(mTimer);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                String param = "36646334"; //hex equivalent to "6dc4"
                edtWrittenValue.setText(param + levelControlTail);
                param = levelControlHead + param + levelControlTail;

                long v0 = 0;
                long v1 = 0;
                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:07") || mDeviceBoardAddress.equals("34:81:F4:15:1D:09")
                        || mDeviceBoardAddress.equals("34:81:F4:15:1D:37") || mDeviceBoardAddress.equals("34:81:F4:15:1D:28")){
                    //A10601366463340100
                    //int of 06013664
                    v0 = 100742756;
                    //int of 63340100
                    v1 = 1664352512;
                }
                String concatedString = encryptAndWrite(v0, v1);
//                if (mDeviceBoardAddress.equals("34:81:F4:15:1D:37")){
//                    mBleService.writeCharacteristic(ledSensorTest, hexStringToByteArray(param));
//                } else {
//                    mBleService.writeCharacteristic(ledSensor, hexStringToByteArray(param));
//                }

                if (mRead) {
                    mHandler.postDelayed(mTimer, 20);
                }
            }
        });

        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowUserDialog("ADD");
            }
        });

        btnDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowUserDialog("DELETE");
            }
        });

        btnDeleteAllUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowUserDialog("DELETEALL");
            }
        });

        btnSyncDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date now = new Date();

                //year
                SimpleDateFormat sdfYear = new SimpleDateFormat("yy", Locale.ENGLISH);
                String year = sdfYear.format(now);

                //month
                SimpleDateFormat sdfMonth = new SimpleDateFormat("MM", Locale.ENGLISH);
                String month = sdfMonth.format(now);

                //dayDate (i.e 25)
                SimpleDateFormat sdfDayDate = new SimpleDateFormat("dd", Locale.ENGLISH);
                String dayDate = sdfDayDate.format(now);

                //dayWeek (i.e 01 is monday, 07 is sunday)
                SimpleDateFormat sdfDayWeek = new SimpleDateFormat("E", Locale.ENGLISH);
                String dayWeek = sdfDayWeek.format(now);

                if (dayWeek.equalsIgnoreCase("MON"))
                    dayWeek = "01";
                else if (dayWeek.equalsIgnoreCase("TUE"))
                    dayWeek = "02";
                else if (dayWeek.equalsIgnoreCase("WED"))
                    dayWeek = "03";
                else if (dayWeek.equalsIgnoreCase("THU"))
                    dayWeek = "04";
                else if (dayWeek.equalsIgnoreCase("FRI"))
                    dayWeek = "05";
                else if (dayWeek.equalsIgnoreCase("SAT"))
                    dayWeek = "06";
                else if (dayWeek.equalsIgnoreCase("SUN"))
                    dayWeek = "07";

                //hour - 24 hour format
                SimpleDateFormat sdfHour = new SimpleDateFormat("HH", Locale.ENGLISH);
                String hour = sdfHour.format(now);

                //minute
                SimpleDateFormat sdfMinute = new SimpleDateFormat("mm", Locale.ENGLISH);
                String minute = sdfMinute.format(now);

                //second
                SimpleDateFormat sdfSecond = new SimpleDateFormat("ss", Locale.ENGLISH);
                String second = sdfSecond.format(now);

                String param = year + month + dayDate + dayWeek + hour + minute + second;
                edtWrittenValue.setText("yymmdddwhhmmss\n" + param + "\n(" + now + ")");

                param = timeControlHead + param;
                mBleService.writeCharacteristic(ledSensor, hexStringToByteArray(param));
            }
        });

        btnShowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llLiftControl.setVisibility(View.GONE);
                svControl.setVisibility(View.GONE);
                llLv.setVisibility(View.VISIBLE);
                isGone = true;
                //progressBar.setVisibility(View.VISIBLE);
                // loading.setVisibility(View.VISIBLE);


            }
        });

        btnReadValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = mBleService.getValue(ledSensor, 3);

                if (data == null) {
                    edtReadValue.setText(R.string.txt_no_data_written);
                } else if (data.isEmpty()) {
                    edtReadValue.setText(R.string.txt_empty_data);
                } else {
                    //current data is in ascii, convert it to hex string
                    data = asciiToHex(data);

                    edtReadValue.setText(data);
                }
            }
        });

        btnCard1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowConfigurationDialog("01");
            }
        });

        btnCard2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowConfigurationDialog("02");
            }
        });

        btnCard3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowConfigurationDialog("03");
            }
        });

        btnCard4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowConfigurationDialog("04");
            }
        });
    }


    public void ShowUserDialog(final String type) {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_user, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        alertDialogBuilder.setCancelable(false);

        final LinearLayout llRoot = (LinearLayout) promptsView.findViewById(R.id.layout_root);
        final Button btnOk = (Button) promptsView.findViewById(R.id.btn_ok);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btn_cancel);
        final TextView tvTitle = (TextView) promptsView.findViewById(R.id.tv_title);
        final TextView tvMsg = (TextView) promptsView.findViewById(R.id.tv_msg);
        final EditText edtInput = (EditText) promptsView.findViewById(R.id.edt_input);

        try {
            if (type.equals("ADD")) {
                tvTitle.setText(R.string.txt_add_user);
                tvMsg.setText(R.string.txt_enter_user_id);
            } else if (type.equals("DELETE")) {
                tvTitle.setText(R.string.txt_delete_user);
                tvMsg.setText(R.string.txt_enter_user_id);
            } else if (type.equals("DELETEALL")) {
                tvTitle.setText(R.string.txt_delete_all_users);
                tvMsg.setText(R.string.txt_confirm_delete_all_users);
                edtInput.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // create alert dialog
        ShowAlertDialog = alertDialogBuilder.create();
        ShowAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // show it
        ShowAlertDialog.show();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("ADD") || type.equals("DELETE")) {
                    edtInput.setError(null);

                    String input = edtInput.getText().toString();

                    if (TextUtils.isEmpty(input)) {
                        edtInput.setError("This field is required.");
                        edtInput.requestFocus();
                    } else {
                        String param = userControlHead;

                        if (input.length() < 6) {
                            int remain = 6 - input.length();

                            for (int i = 0; i < remain; i++) {
                                input = "0" + input;
                            }
                        }

                        if (type.equals("ADD")) {
                            param = param + "01" + input;
                            //edtWrittenValue.setText("01" + input);
                        } else if (type.equals("DELETE")) {
                            param = param + "00" + input;
                            //edtWrittenValue.setText("00" + input);
                        }

                        edtWrittenValue.setText(input);
                        mBleService.writeCharacteristic(ledSensor, hexStringToByteArray(param));

                        ShowAlertDialog.dismiss();
                    }
                } else if (type.equals("DELETEALL")) {
                    String param = userControlHead + "FF";

                    edtWrittenValue.setText(param);

                    mBleService.writeCharacteristic(ledSensor, hexStringToByteArray(param));

                    ShowAlertDialog.dismiss();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAlertDialog.dismiss();

            }
        });
    }

    public void ShowConfigurationDialog(final String index) {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_card_configuration, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        //alertDialogBuilder.setTitle("Login");

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        alertDialogBuilder.setCancelable(false);

        final LinearLayout llRoot = (LinearLayout) promptsView.findViewById(R.id.layout_root);
        final TextView tvPrompt = (TextView) promptsView.findViewById(R.id.tv_prompt);
        final Button btnLv1 = (Button) promptsView.findViewById(R.id.btn_level_1);
        final Button btnLv2 = (Button) promptsView.findViewById(R.id.btn_level_2);
        final Button btnLv3 = (Button) promptsView.findViewById(R.id.btn_level_3);
        final Button btnLv4 = (Button) promptsView.findViewById(R.id.btn_level_4);
        final Button btnLv5 = (Button) promptsView.findViewById(R.id.btn_level_5);
        final Button btnLv6 = (Button) promptsView.findViewById(R.id.btn_level_6);
        final Button btnLv7 = (Button) promptsView.findViewById(R.id.btn_level_7);
        final Button btnLv8 = (Button) promptsView.findViewById(R.id.btn_level_8);
        final Button btnLv9 = (Button) promptsView.findViewById(R.id.btn_level_9);
        final Button btnLv10 = (Button) promptsView.findViewById(R.id.btn_level_10);
        final Button btnLv11 = (Button) promptsView.findViewById(R.id.btn_level_11);
        final Button btnLv12 = (Button) promptsView.findViewById(R.id.btn_level_12);
        final Button btnLv13 = (Button) promptsView.findViewById(R.id.btn_level_13);
        final Button btnLv14 = (Button) promptsView.findViewById(R.id.btn_level_14);
        final Button btnLv15 = (Button) promptsView.findViewById(R.id.btn_level_15);
        final Button btnLv16 = (Button) promptsView.findViewById(R.id.btn_level_16);
        final Button btnClearLevel = (Button) promptsView.findViewById(R.id.btn_clear_level);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btn_cancel);

        try {
            tvPrompt.setText(getResources().getString(R.string.txt_select_configuration).replace("[index]", index));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // create alert dialog
        ShowAlertDialog = alertDialogBuilder.create();
        ShowAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // show it
        ShowAlertDialog.show();

        btnLv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "01");
            }
        });

        btnLv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "02");
            }
        });

        btnLv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "03");
            }
        });

        btnLv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "04");
            }
        });

        btnLv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "05");
            }
        });

        btnLv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "06");
            }
        });

        btnLv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "07");
            }
        });

        btnLv8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "08");
            }
        });

        btnLv9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "09");
            }
        });

        btnLv10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "0A");
            }
        });

        btnLv11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "0B");
            }
        });

        btnLv12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "0C");
            }
        });

        btnLv13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "0D");
            }
        });

        btnLv14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "0E");
            }
        });

        btnLv15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "0F");
            }
        });

        btnLv16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "10");
            }
        });

        btnClearLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCardConfiguration(index, "00");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAlertDialog.dismiss();

            }
        });
    }

    private String encryptAndWrite(long v0, long v1){
        String returnedEncrypt = encryptData(v0 , v1); // need return value hex string or unhex string
        Log.d(TAG, "onReturnChar: " + returnedEncrypt);

        //Split the returned long string into two parts
        String [] splitEncrypt = returnedEncrypt.split(",");
        String firstPartEncrypt = splitEncrypt[0];
        String secondPartEncrypt = splitEncrypt[1];


        //Parse each part of the string to long.
        long p1HexLong = Long.parseLong(firstPartEncrypt);
        long p2HexLong = Long.parseLong(secondPartEncrypt);


        String was =Integer.toBinaryString((int) p1HexLong);
        String wes = Integer.toBinaryString((int) p2HexLong);

        Log.d(TAG, "binary part1: " + was);
        Log.d(TAG, "binary part2: " + wes);

        String convertHex1 = Long.toHexString(Long.parseLong(was, 2)).toUpperCase();
        String convertHex2 = Long.toHexString(Long.parseLong(wes, 2)).toUpperCase();

        if (convertHex1.length() < 8 && convertHex1.length() == 7){
            convertHex1 = "0" + convertHex1;
        }
        if (convertHex2.length() < 8 && convertHex2.length() == 7){
            convertHex2 = "0" + convertHex2;
        }
        Log.d(TAG, "returnHex: " + convertHex1);
        Log.d(TAG, "returnHex: " + convertHex2);

        //Add back the A1 first command taken out before encryption
        //String concatHexLong = "A1" + convertHex1 + convertHex2;
        String concatHexLong = "A1" + convertHex1 + convertHex2;
        Log.d(TAG, "onConvertBiz: " + concatHexLong);

        //Write the characteristic
        if (ledSensorWrite != null) {
            Log.d(TAG, "returnsensor: " + ledSensorWrite.getUuid().toString());
            mBleService.writeCharacteristic(ledSensorWrite, hexStringToByteArrayEncrypt(concatHexLong));
        }
        else {
            Log.d(TAG, "returnsensorzz: " + ledSensor);
            mBleService.writeCharacteristic(ledSensor, hexStringToByteArrayEncrypt(concatHexLong));
        }

        return concatHexLong;
    }

    private void setCardConfiguration(String index, String config) {
        String param = cardControlHead + index + config;
        mBleService.writeCharacteristic(ledSensor, hexStringToByteArray(param));

        ShowAlertDialog.dismiss();

        Toast.makeText(this, getResources().getString(R.string.configuration_complete).replace("[index]", index),
                Toast.LENGTH_SHORT).show();
    }

    private static String asciiToHex(String s) {
        char[] chars = s.toCharArray();
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int) chars[i]));
        }
        return hex.toString();
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Log.i("Permission", "Granted");

                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_ENABLE_LOCATION);
                    }
                }
                return;
            }
        }
    }

    public void getBLETransactionLog(){
        if (CommonUtilities.isConnectionAvailable(DeviceControlActivity.this)) {
            if(CommonUtilities.pingHost()) {
                new addBleLogTask(mAction, token, 0, concater).execute();
                CommonUtilities.stoprunning = false;
            }else{
                CommonUtilities.dismissProgress();
                String message = "Lost Connection to host";
                CommonUtilities.showAlertDialog(DeviceControlActivity.this, message, false);
            }
        } else {
            CommonUtilities.dismissProgress();
            String message = "No internet connection";
            CommonUtilities.showAlertDialog(DeviceControlActivity.this, message, false); // if dont want close put 'false'
        }
    }

    private class addBleLogTask extends AsyncTask<Void, Void, Void> {

        WebService ws = new WebService();
        private final String sAction;
        private final String sToken;
        private final int iBLE2Id;
        private  String arrLogs;


        addBleLogTask(String mAction , String mToken , int mBLE2Id , String mArrLogs) {
            sAction = mAction;
            sToken = mToken;
            iBLE2Id = mBLE2Id;
            arrLogs = mArrLogs;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            parameters = new RequestParams();
            parameters.put("sAction", sAction);
            parameters.put("sToken", sToken);
            parameters.put("iBLE2Id" , iBLE2Id);
            parameters.put("arrLogs" , arrLogs);


            // Invoke REST Web service to check visitor
            result = ws.invokeWS(parameters);

            status = Boolean.parseBoolean(result.get("success").toString());

            message = result.get("error").toString();

            Log.d("RESPONSE", String.valueOf(result));

            Log.d(TAG, "status: " + status);

            if (status) {

                if(sAction.equals(mAction))
                {

                }

            }
            else {
                Log.e(TAG, "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(status) {
                try {
                    // Toast.makeText(DeviceControlActivity.this, "Sudah", Toast.LENGTH_SHORT).show();
                } catch (Exception exp)  {
                    Log.e(TAG, "onPostExecute Error:" + exp.getMessage());
                }
                flag_loading = false;
            }
        }
    }

    //Async Task

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, R.string.error_bluetooth_off, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBleService != null) {
            if (mBleService.serviceStatus) {
                unbindService(mServiceConnection);
            }
            mBleService.close();
            mBleService.disconnect();
            mBleService = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRead) {
            mHandler.removeCallbacks(mTimer);
        }

        unregisterReceiver(bluetoothReceiver);
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        if (ledSensor == null) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static String decToHex(long dec)
    {
        return Long.toHexString(dec);
    }

    public static byte[] hexStringToByteArrayEncrypt(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static String bytesToHex(byte[] bytes) {

        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BLEService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BLEService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }
}
